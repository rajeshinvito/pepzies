//
//  ForgotPasswordVC.swift
//  Papzies
//
//  Created by apple on 17/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import CountryPickerView
import NVActivityIndicatorView

class ForgotPasswordVC: UIViewController,NVActivityIndicatorViewable,CountryPickerViewDataSource,CountryPickerViewDelegate {
    

    @IBOutlet weak var btnBackToLogin: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    var countryCode = ""
    var fromLogin = false
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if fromLogin{
            btnBackToLogin.isHidden = true
        }
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 100  , height: txtMobileNumber.frame.size.height))
        cp.flagImageView.isHidden = false
        cp.showCountryCodeInView = true
//        cp.flagSpacingInView = -10
        cp.font = UIFont(name: "Apercu Medium", size:36.0)
        cp.isUserInteractionEnabled = true
        txtMobileNumber.leftView = cp
        txtMobileNumber.leftViewMode = .always
        cp.textColor = .white
        countryCode = cp.selectedCountry.phoneCode
        cp.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cmdSend(_ sender: Any) {
        if txtMobileNumber.text == ""{
            self.Alert(title: "Alert!", message: "Please enter mobile number.")
        }else{
            self.forgotPassword()

        }

    }
    @IBAction func cmdBackToLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.countryCode = country.phoneCode
    }

    
    func forgotPassword(){
        
        let param:[String:Any] = ["mobile":"\(self.countryCode)\(txtMobileNumber.text!)"]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "forgot-password", parameters: param, bearerToken: ""){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            self.view.endEditing(true)
                                let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordOTPVC") as! ForgotPasswordOTPVC
                            Vc.mobileNumber = "\(self.countryCode)\(self.txtMobileNumber.text!)"
                                self.navigationController?.pushViewController(Vc, animated: true)

                        }
                    }
                }
            }
        }
    }
}
