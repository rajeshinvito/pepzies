//
//  WalletVC.swift
//  Papzies
//
//  Created by apple on 17/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class WalletVC: UIViewController {

    @IBOutlet weak var btnMenu: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func cmdMenu(_ sender: Any) {
        sharedMenuClass.menuObj.addMenuClass(view: self.view)
        sharedMenuClass.menuObj.menuView.delegate = self

    }
    
}

extension WalletVC:MenuDelegate {
    func callMethod1(vcIndex: Int) {
        navigationClass1(index:vcIndex)
    }
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }

    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}



