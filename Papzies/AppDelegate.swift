//
//  AppDelegate.swift
//  Papzies
//
//  Created by apple on 09/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NotificationCenter
import UserNotifications
import IQKeyboardManagerSwift
import Firebase
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyBr73R7SkyZkGSbLdeDEwl6n0jPC8SBfJo")
        GMSPlacesClient.provideAPIKey("AIzaSyBr73R7SkyZkGSbLdeDEwl6n0jPC8SBfJo")
        setupIQKeyboardManager()
        
        
//        let _ = UserLocationManager.SharedManager
        UNUserNotificationCenter.current().delegate = self
        if #available(iOS 10.0, *) {
            
            let center = UNUserNotificationCenter.current
            center().requestAuthorization(options: [.badge, .alert, .sound], completionHandler: { (granted, error) in
                
            })
            
            application.registerForRemoteNotifications()
            
        } else {
            
            
            let types: UIUserNotificationType = [.alert, .badge, .sound]
            let settings = UIUserNotificationSettings(types: types, categories: nil)
            application.registerUserNotificationSettings(settings)
            
        }
        navigateToPage()
        
        return true
        
    }
    
    
    func navigateToPage(){
        
        let login = UserDefaults.standard.bool(forKey: "LoggedIn")
        var navigation = UINavigationController()
        var mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if login{//
            let role = UserDefaults.standard.string(forKey: UserConstant.user_type)
            if role == "1"{
                mainStoryboardIpad = UIStoryboard(name: "UserStoryboard", bundle: nil)
                let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
                self.window = UIWindow(frame: UIScreen.main.bounds)
                navigation = UINavigationController(rootViewController: initialViewControlleripad)
                
            }else{
                
                
                let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "PapziesHomeVC") as! PapziesHomeVC
                self.window = UIWindow(frame: UIScreen.main.bounds)
                navigation = UINavigationController(rootViewController: initialViewControlleripad)
                
            }
        }else{
            
            let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.window = UIWindow(frame: UIScreen.main.bounds)
            navigation = UINavigationController(rootViewController: initialViewControlleripad)
        }
        self.window?.rootViewController = navigation
        self.window?.makeKeyAndVisible()
        
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
        // 2. Print device token to use for PNs payloads
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // 1. Print out error if PNs registration not successful
        print("Failed to register for remote notifications with error: \(error)")
    }
    
    func application(_ application: UIApplication,didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler:
        @escaping (UIBackgroundFetchResult) -> Void) {
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        completionHandler()
    }
    func setupIQKeyboardManager(){
          IQKeyboardManager.shared.enable = true
          IQKeyboardManager.shared.resignFirstResponder()
          IQKeyboardManager.shared.enableAutoToolbar = true
          IQKeyboardManager.shared.shouldResignOnTouchOutside = true
      }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let applicationState = UIApplication.shared.applicationState
        if applicationState == .active{
            
        }else{
            let userInfo = notification.request.content.userInfo
            //let notificationType = userInfo["notificationType"] as! String
            print(userInfo)
            completionHandler([.alert,.sound])
            
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        let login = UserDefaults.standard.bool(forKey: "LoggedIn")
        if login{
            SocketIOManager.sharedInstance.establishConnection()
        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
