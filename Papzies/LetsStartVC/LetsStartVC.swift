//
//  LetsStartVC.swift
//  Papzies
//
//  Created by apple on 12/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class LetsStartVC: UIViewController {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var btnStart: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isHidden = true
        let userType = UserDefaults.standard.string(forKey: UserConstant.user_type)
        if userType == "1"{
            backgroundImage.image = UIImage(named: "userType1")
        }else{
            backgroundImage.image = UIImage(named: "userType2")
        }
        UserDefaults.standard.set(true, forKey: "LoggedIn")
        UserDefaults.standard.synchronize()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cmdStart(_ sender: Any) {
        let role = UserDefaults.standard.string(forKey: UserConstant.user_type)
        if role == "1"{
//            let Storyboard : UIStoryboard = UIStoryboard(name: "UserStoryboard", bundle: nil)
//            let Vc = Storyboard.instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
//            self.navigationController?.pushViewController(Vc, animated: true)
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "UserStoryboard", bundle:nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "UserDashboardVC") as! UserDashboardVC
            let navigationController = UINavigationController(rootViewController: newViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = navigationController


        }else{
                        
//            let Storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let Vc = Storyboard.instantiateViewController(withIdentifier: "PapziesHomeVC") as! PapziesHomeVC
//            self.navigationController?.pushViewController(Vc, animated: true)
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "PapziesHomeVC") as! PapziesHomeVC
            let navigationController = UINavigationController(rootViewController: newViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = navigationController
        }

    }
}
