//
//  sendercell.swift
//  Papzies
//
//  Created by APPLE  on 17/03/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class sendercell: UITableViewCell {

    @IBOutlet weak var senderlblref: UILabel!
    @IBOutlet weak var senderimgref: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       senderlblref.roundCorners(corners: [.topLeft,.bottomRight], radius: 10.0)
       senderlblref.padding = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 20)
         self.layoutIfNeeded()
       
    }
    override func layoutSubviews() {
        super.layoutSubviews()
      //lblBackviewref.roundCorners(corners: [.topLeft,.bottomRight], radius: 10.0)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
