//
//  ChooseRoleVC.swift
//  Papzies
//
//  Created by apple on 09/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ChooseRoleVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBecomePapzies: UIButton!
    @IBOutlet weak var btnHirePapzies: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = true
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)

        // Do any additional setup after loading the view.
    }
    //MARK:-   screen left end swift for back
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func cmdHirePapzies(_ sender: Any) {
        UserDefaults.standard.set("1", forKey: UserConstant.user_type)

        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PapziesSignupVC") as! PapziesSignupVC
        Vc.user_Type = "1"
        self.navigationController?.pushViewController(Vc, animated: true)

    }
    
    @IBAction func cmdBecomePapzies(_ sender: Any) {
        UserDefaults.standard.set("2", forKey: UserConstant.user_type)
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PapziesSignupVC") as! PapziesSignupVC
        Vc.user_Type = "2"
        self.navigationController?.pushViewController(Vc, animated: true)

    }
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
