//
//  ForgotPasswordOTPVC.swift
//  Papzies
//
//  Created by apple on 17/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ForgotPasswordOTPVC: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtFourth: UIKerningTextField!
    @IBOutlet weak var txtThird: UIKerningTextField!
    @IBOutlet weak var txtSecond: UIKerningTextField!
    @IBOutlet weak var txtFirst: UIKerningTextField!
    @IBOutlet weak var btnBack: UIButton!
    var mobileNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHeaderTitle.text = "Please enter the OTP send to your mobile number \(mobileNumber)"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cmdSubmit(_ sender: Any) {
        self.textFieldValidation()

    }
    @IBAction func cmdResend(_ sender: Any) {
        self.reSendOTP()
    }
    
    func textFieldValidation(){
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            if txtField.isEmpty{
                self.Alert(title: "Alert!", message: "Please enter correct verification code")
                return
            }
        }
        self.verifyMobileOTP()
    }
    


    
    func reSendOTP(){
        
        let param:[String:Any] = ["mobile":mobileNumber]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "forgot-password", parameters: param, bearerToken: ""){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }
                    }
                }
            }else{
                self.Alert(title: "Alert!", message: "Something went wrong!")
            }
        }
    }

    func verifyMobileOTP(){
        let OTP = "\(txtFirst.text!)\(txtSecond.text!)\(txtThird.text!)\(txtFourth.text!)"
        let param:[String:Any] = ["token":OTP,"mobile":mobileNumber]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "verify-forgot-password", parameters: param, bearerToken: ""){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                                let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
                                Vc.mobileNumber = self.mobileNumber
                                Vc.token = OTP
                                self.navigationController?.pushViewController(Vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
}

extension ForgotPasswordOTPVC:UITextFieldDelegate{
    
    @IBAction func textEditDidBegin(_ sender: UITextField) {
        print("textEditDidBegin has been pressed")
        
        if !(sender.text?.isEmpty)!{
            sender.selectAll(self)
            //buttonUnSelected()
        }else{
            print("Empty")
            sender.text = " "
            
        }
        
    }
    @IBAction func textEditChanged(_ sender: UITextField) {
        print("textEditChanged has been pressed")
        let count = sender.text?.count
        //
        if count == 1{
            
            switch sender {
            case txtFirst:
                txtSecond.becomeFirstResponder()
            case txtSecond:
                txtThird.becomeFirstResponder()
            case txtThird:
                txtFourth.becomeFirstResponder()
            case txtFourth:
                txtFourth.resignFirstResponder()
            default:
                print("default")
            }
        }
        
    }


    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        textField.text = ""
        if textField.text == "" {
            print("Backspace has been pressed")
        }
        
        if string == ""
        {
            print("Backspace was pressed")
            switch textField {
            case txtSecond:
                txtFirst.becomeFirstResponder()
            case txtThird:
                txtSecond.becomeFirstResponder()
            case txtFourth:
                txtThird.becomeFirstResponder()
            default:
                print("default")
            }
            textField.text = ""
            return false
        }
        
        return true
}
}
