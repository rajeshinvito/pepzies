//
//  CustomFunctions.swift
//  Papzies
//
//  Created by apple on 10/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import CoreLocation


extension UIViewController {
    
    func calculateDistancebetweenCordinate(fist:CLLocation, second:CLLocation) -> CLLocationDistance {
        
        let distanceInMeters = fist.distance(from: second)
        return distanceInMeters
        
    }
    
    func Alert(title:String, message:String){
        
        let alert = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: "") , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    func getTextfield(view: UIView) -> [UITextField] {
        var results = [UITextField]()
        for subview in view.subviews as [UIView] {
            if let textField = subview as? UITextField {
                results += [textField]
            }else {
                results += getTextfield(view: subview)
            }
        }
        return results
    }
    
    func addRippleEffect(to referenceView: UIView) {
        /*! Creates a circular path around the view*/
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height), cornerRadius: 20.0)
        let shapePosition = CGPoint(x: referenceView.bounds.size.width / 2.0, y: referenceView.bounds.size.height / 2.0)
        let rippleShape = CAShapeLayer()
        rippleShape.bounds = CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height)
        rippleShape.path = path.cgPath
        rippleShape.fillColor = UIColor.clear.cgColor
        rippleShape.strokeColor = UIColor.white.cgColor
        rippleShape.lineWidth = 1
        rippleShape.position = shapePosition
        rippleShape.opacity = 0
        
        
        /*! Add the ripple layer as the sublayer of the reference view */
        referenceView.layer.addSublayer(rippleShape)
        /*! Create scale animation of the ripples */
        let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
        scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(2, 20, 1))
        /*! Create animation for opacity of the ripples */
        let opacityAnim = CABasicAnimation(keyPath: "opacity")
        opacityAnim.fromValue = 1
        opacityAnim.toValue = nil
        /*! Group the opacity and scale animations */
        let animation = CAAnimationGroup()
        animation.animations = [scaleAnim, opacityAnim]
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.duration = CFTimeInterval(0.7)
        animation.repeatCount = 25
        animation.isRemovedOnCompletion = true
        rippleShape.add(animation, forKey: "rippleEffect")
    }
    
}

struct NearByPapzies {
    
    var id:String = ""
    var fname:String = ""
    var lname:String = ""
    var email:String = ""
    var mobile:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var city:String = ""
    var image:String = ""
    var state:String = ""
    var country:String = ""
    var address:String = ""
    var zip:String = ""
    var bio:String = ""
    var rating:String = ""
    var distance:String = ""
    var images:[String] = [""]
    
    init(value:Dictionary<String,Any>,dist:String) {
        
        self.id = value["id"] as? String ?? ""
        self.fname = value["fname"] as? String ?? ""
        self.lname = value["lname"] as? String ?? ""
        self.email = value["email"] as? String ?? ""
        self.mobile = value["mobile"] as? String ?? ""
        self.latitude = value["latitude"] as? String ?? ""
        self.longitude = value["longitude"] as? String ?? ""
        self.city = value["city"] as? String ?? ""
        self.image = value["image_url"] as? String ?? ""
        self.state = value["state"] as? String ?? ""
        self.country = value["country"] as? String ?? ""
        self.address = value["address"] as? String ?? ""
        self.zip = value["zip"] as? String ?? ""
        self.bio = value["bio"] as? String ?? ""
        self.rating = value["rating"] as? String ?? ""
        self.distance = dist
        self.images = value["images"] as? [String] ?? [""]
        
    }
}

struct AllMarker {
    var lat: Double
    var long: Double
    var name: String
}

