//
//  ConstantString.swift
//  Papzies
//
//  Created by apple on 10/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class colorHex {
    
    static let black = "#222222"
    
}


class UserConstant {
    
    static let MapboxAccessToken = "pk.eyJ1IjoicGFwemllcyIsImEiOiJjazVjMGh2MWYwaHJhM2VvNThtajJqeXhiIn0.TSMhGtTBPM-5ReI9MWnqCw"
    static let user_Id = "user_Id"
    static let email = "email"
    static let profile_pic = "profile_pic"
    static let mobile = "mobile"
    static let name = "name"
    static let token = "token"
    static let state = "state"
    static let city = "city"
    static let address = "address"
    static let country = "country"
    static let user_type = "user_type"
    static let bearer_Token = "bearer_Token"
    static let login_Status = "login_Status"
    static let User_Rating =  "User_Rating"
    static let Session_id = "Session_id"
    static let Session_Status = "Session_Status"
    static let type =  "type"
}
