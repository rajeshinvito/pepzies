//
//  UItextField.swift
//  Papzies
//
//  Created by apple on 10/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit


extension UITextField {
    
    @IBInspectable
    var placeHolderColor: UIColor? {
        get {
            return .black
        }
        set {
            if let color = newValue {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                                attributes: [NSAttributedString.Key.foregroundColor: color])
                
            } else {
                layer.shadowColor = nil
            }
        }
    }

    
    var isEmpty: Bool {
        if let text = self.text, !text.isEmpty {
             return false
        }
        return true
    }
    
    func addBorder(_ width: CGFloat, color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        
    }
    
    func addLeftPadding() {
        let textField           = self
        textField.leftViewMode  = .always
        let height = textField.frame.size.height
        
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 18, height: height))
        //textField.leftView      = UIView(frame: CGRectMake(0, 0, 8, CGRectGetHeight(textField.frame)))
    }
        
//    func isEmpty() -> Bool {
//        return self.text?.count == 0 ? true : false
//    }
    
    func count() -> Int {
        
        return self.text!.trimmingCharacters(in: .whitespacesAndNewlines).count
        
    }
    
    func isValidEmail() -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
    func validatePhoneNumber() -> Bool {
        let phoneNumberRegex = "^[6-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self.text!)
        return isValidPhone
    }
    
}

class TextField: UITextField {
    
    var indexPath : IndexPath!
    var Tagg : Int!
    var name:String!
    
    let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
//    @IBInspectable
//    var placeHolderColor: UIColor? {
//        get {
//            return .black
//        }
//        set {
//            if let color = newValue {
//                self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
//                                                                attributes: [NSAttributedString.Key.foregroundColor: color])
//
//            } else {
//                layer.shadowColor = nil
//            }
//        }
//    }
    
//    @IBInspectable var letterSpace: Float {
//        get {
//            var range = NSMakeRange(0, (text ?? "").count)
//            guard let kern = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: &range),
//                let value = kern as? NSNumber
//                else {
//                    return 0
//            }
//            return value.floatValue
//        }
//        set {
//            var attText:NSMutableAttributedString
//
//            if let attributedText = attributedText {
//                attText = NSMutableAttributedString(attributedString: attributedText)
//            } else if let text = text {
//                attText = NSMutableAttributedString(string: text)
//            } else {
//                attText = NSMutableAttributedString(string: "")
//            }
//
//            let range = NSMakeRange(0, attText.length)
//            attText.addAttribute(NSAttributedString.Key.kern, value: NSNumber(value: newValue), range: range)
//            self.attributedText = attText
//        }
//    }
    
}

extension UITextView{
    
    @IBInspectable var letterSpace: Float {
        get {
            var range = NSMakeRange(0, (text ?? "").count)
            guard let kern = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: &range),
                let value = kern as? NSNumber
                else {
                    return 0
            }
            return value.floatValue
        }
        set {
            var attText:NSMutableAttributedString

            if let attributedText = attributedText {
                attText = NSMutableAttributedString(attributedString: attributedText)
            } else if let text = text {
                attText = NSMutableAttributedString(string: text)
            } else {
                attText = NSMutableAttributedString(string: "")
            }

            let range = NSMakeRange(0, attText.length)
            attText.addAttribute(NSAttributedString.Key.kern, value: NSNumber(value: newValue), range: range)
            self.attributedText = attText
        }
    }

}



import UIKit

extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }

    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }

    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: rect.inset(by: insets))
        } else {
            self.drawText(in: rect)
        }
    }

    override open var intrinsicContentSize: CGSize {
        guard let text = self.text else { return super.intrinsicContentSize }

        var contentSize = super.intrinsicContentSize
        var textWidth: CGFloat = frame.size.width
        var insetsHeight: CGFloat = 0.0
        var insetsWidth: CGFloat = 0.0

        if let insets = padding {
            insetsWidth += insets.left + insets.right
            insetsHeight += insets.top + insets.bottom
            textWidth -= insetsWidth
        }

        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                        attributes: [NSAttributedString.Key.font: self.font], context: nil)

        contentSize.height = ceil(newSize.size.height) + insetsHeight
        contentSize.width = ceil(newSize.size.width) + insetsWidth

        return contentSize
    }
}
