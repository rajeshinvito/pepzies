//
//  LocationServices.swift
//

import Foundation
import CoreLocation

protocol LocationUpdateProtocol {
    func locationDidUpdateToLocation(location : CLLocation)
}

/// Notification on update of location. UserInfo contains CLLocation for key "location"
let kLocationDidChangeNotification = "LocationDidChangeNotification"

class UserLocationManager: NSObject, CLLocationManagerDelegate {
    
    static let SharedManager = UserLocationManager()
    
    var locationManager = CLLocationManager()
    
    var currentLocation : CLLocation?
    
    var delegate : LocationUpdateProtocol?
    
    private override init () {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = 0
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func stopLocation(){
        self.locationManager.stopUpdatingLocation()
        self.locationManager.stopMonitoringSignificantLocationChanges()

    }
        func StartLocation(){
            self.locationManager.delegate = self
            self.locationManager.distanceFilter = 50
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
            self.locationManager.allowsBackgroundLocationUpdates = true
            self.locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.requestAlwaysAuthorization()
            self.locationManager.startUpdatingLocation()

        }

    
    // MARK: - CLLocationManagerDelegate
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            self.locationManager.allowsBackgroundLocationUpdates = true
            self.locationManager.pausesLocationUpdatesAutomatically = false
        
        currentLocation = locations.first
        let userInfo = ["location" : currentLocation!]
        self.delegate?.locationDidUpdateToLocation(location: self.currentLocation!)

        DispatchQueue.main.async {
            UserDefaults.standard.set(self.currentLocation?.coordinate.latitude, forKey: "Lat")
            UserDefaults.standard.set(self.currentLocation?.coordinate.longitude, forKey: "Long")
            UserDefaults.standard.synchronize()
            let login = UserDefaults.standard.bool(forKey: "LoggedIn")
            if login{//
                let role = UserDefaults.standard.string(forKey: UserConstant.user_type)
                let userId = UserDefaults.standard.string(forKey: UserConstant.user_Id)
                if role != "1"{
                     let sessionId = UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? ""
                    let loginStatus = UserDefaults.standard.bool(forKey: UserConstant.login_Status) 
                    if loginStatus{
                        SocketIOManager.sharedInstance.updatePaziesLocation(latitude: "\(self.currentLocation?.coordinate.latitude ?? 0.0)" , longitude: "\(self.currentLocation?.coordinate.longitude ?? 0.0)" , id: userId!, sessionId: sessionId)

                    }
                }

            //NotificationCenter.default.post(name: NSNotification.Name.init(kLocationDidChangeNotification), object: self, userInfo: userInfo)
            
        }
    }
}
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            self.locationManager.startUpdatingLocation()
            
        case .authorizedWhenInUse:
            self.locationManager.startUpdatingLocation()
            
        case .denied:
            break
            
        case .notDetermined:
             break
        case .restricted:
             break
        
        }
    }
    
}


