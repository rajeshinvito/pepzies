//
//  ModalClass.swift
//  Crete-Link
//
//  Created by osx on 20/08/18.
//  Copyright © 2018 O.com. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration

let sessions = UserDefaults()

class alertManager: NSObject {
    
    static  let callAlert = alertManager()
    
    private override init() {
        super.init()
    }
    
    func show(controller:UIViewController,message:String) -> Void {
        
        
    }
    
    func checkErrorCodes (controller:UIViewController,error:NSError) {
        if error.code == -1009 {
        }
        else if error.code == 3840 {
        }
        else if error.code == 4 {
        }
        else {
        }
    }
}

class APIClass: NSObject {
    
    var alamoFireManager : SessionManager?
    let headersLogin: HTTPHeaders = [
        "Content-Type": "application/json"]
    let baseUrlLogin = "http://3.136.221.59:3000/"
    
    static let apiClass = APIClass()
    private override init() {
    }
    func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func PostRequest2(api:String, parameters:[String:Any],bearerToken:String, completionHandler: @escaping ((_ json: AnyObject?, _ success:Bool) -> Void)) {
        do{
            if isConnectedToNetwork()  {
                
                let configuration = URLSessionConfiguration.default
                
                var headerMessage :HTTPHeaders!
                if bearerToken != ""{
                    headerMessage = [
                        "Content-Type":"application/x-www-form-urlencoded","Accept":"application/json","Authorization":bearerToken ]
                }else{
                    headerMessage = [
                        "Content-Type":"application/x-www-form-urlencoded","Accept":"application/json" ]
                }
                let url =   "\(baseUrlLogin)\(api)"
                alamoFireManager = Alamofire.SessionManager(configuration: configuration) // not in this line
                
                alamoFireManager?.request(url, method: .post, parameters: parameters, encoding:  URLEncoding.default, headers: headerMessage).responseJSON {(response:DataResponse<Any>) in
                    if !response.result.isSuccess {
                        completionHandler(nil, false)
                    }
                    if response.response?.statusCode == 200 {
                        if let json = response.result.value  {
                            completionHandler(json as AnyObject, true)
                        }
                    }else if response.response?.statusCode == 201 {
                        if let json = response.result.value  {
                            completionHandler(json as AnyObject, true)
                        }
                    } else if response.response?.statusCode == 401 {
                        if let json = response.result.value  {
                            completionHandler(json as AnyObject, true)
                        }
                    }else if response.response?.statusCode == 422 {
                        if let json = response.result.value  {
                            completionHandler(json as AnyObject, true)
                        }
                    }
                    else {
                        if let json = response.result.value  {
                            completionHandler(json as AnyObject, false)
                        }
                    }
                    }.session.finishTasksAndInvalidate()
                
            }else {
            }
        }catch{
            
        }
    }
    
    func PostRequest(contentType:String, parameters:[String:Any], completionHandler: @escaping ((_ json: AnyObject?, _ success:Bool, _ statusCode:Int) -> Void)) {
        do{
            if isConnectedToNetwork()  {
                
                let configuration = URLSessionConfiguration.default
                
                var headerMessage :HTTPHeaders!
                headerMessage = [
                    "Content-Type":"multipart/form-data","Accept":contentType ]
                
                let url =   "\(baseUrlLogin)"
                alamoFireManager = Alamofire.SessionManager(configuration: configuration) // not in this line
                
                alamoFireManager?.request(url, method: .post, parameters: parameters, encoding:  URLEncoding.default, headers: headerMessage).responseJSON {(response:DataResponse<Any>) in
                    if !response.result.isSuccess {
                        completionHandler(nil, false, 0)
                    }
                    if response.response?.statusCode == 200 {
                        if let json = response.result.value  {
                        completionHandler(json as AnyObject, true, 200)
                        }
                    }else {
                        if let json = response.result.value  {
                            completionHandler(json as AnyObject, false, response.response!.statusCode)
                        }
                    }
                    }.session.finishTasksAndInvalidate()
                
            }else {
            }
        }catch{
            
        }
    }
    
    func getRequest(controller:UIViewController,contentType:String, method:String, embedUrl:String, parameters:[String:Any], completionHandler: @escaping ((_ json: AnyObject?, _ success:Bool) -> Void)) {
        do{
            if isConnectedToNetwork()  {
                
                let configuration = URLSessionConfiguration.default
                
                var headerMessage :HTTPHeaders!
                headerMessage = [
                    "Content-Type":contentType]
                
                let url =   "\(embedUrl)"
                alamoFireManager = Alamofire.SessionManager(configuration: configuration) // not in this line
                
                alamoFireManager?.request(url, method: .get, parameters: [:], encoding:  URLEncoding.default, headers: headerMessage).responseJSON {(response:DataResponse<Any>) in
                    if !response.result.isSuccess {
                        
                        alertManager.callAlert.checkErrorCodes(controller: controller, error:(response.result.error)! as NSError )
                        completionHandler(nil, false)
                        
                    }
                    
                    if response.response?.statusCode == 200 {
                        if let json = response.result.value  {
                            
                            completionHandler(json as AnyObject, true)
                            
                        }
                        else {
                            
                            completionHandler(nil, false)
                            
                        }
                    }
                        
                    else {
                        completionHandler("Unautherised User" as AnyObject?,false)
                        
                    }
                    
                    }.session.finishTasksAndInvalidate()
                
            }else {
            }
        }catch{
            
        }
    }
    
    func requestWith(endUrl: String,fileName:String, imageData: Data?,fileType:String, parameters: [String : Any]?,keyvalue:String,bearerToken:String, completionHandler: @escaping ((_ json: AnyObject?, _ success:Bool) -> Void)){
        
        let url = "http://3.136.221.59:3000/\(endUrl)"
        
        let headers: HTTPHeaders = [
            "Content-type": "application/x-www-form-urlencoded",
            "Accept":"application/json",
            "Authorization":bearerToken
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if let data = imageData{
                multipartFormData.append(data, withName: keyvalue, fileName: fileName, mimeType: fileType)
            }
            
        },
            usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if response.error != nil{
                        completionHandler(response.result.value as AnyObject, false)
                        return
                    }
                    completionHandler(response.result.value as AnyObject, true)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
    //MARK:- Request  multiform data Url With Almofire  *****************************************************************
    func requestPostWithMultiFormData(_ strURL : String,params : [String : String]?,imagesfile: UIImage,keyNameForImage:String,bearerToken:String, completionHandler :@escaping (_ json: AnyObject?, _ success:Bool) -> Void, failure:@escaping (NSError) -> Void){

        var yourHeaders = ["" : ""]
            yourHeaders = [
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                 "Authorization":"Bearer \(bearerToken)"
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
                    if let image = imagesfile as? UIImage{
                        multipartFormData.append(image.jpegData(compressionQuality:0.001)!, withName:keyNameForImage, fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                    }

        }, usingThreshold: UInt64.init(), to:strURL,method:.post,
           headers: yourHeaders){ (encodingResult) in
            switch encodingResult {
             case .success(let upload, _, _):
                upload.responseJSON { (response) in
                    debugPrint(response)
                    if response.error != nil{
                    completionHandler(response.result.value as AnyObject, false)
                    return
                    }
                    completionHandler(response.result.value as AnyObject, true)
                    
                }
                upload.uploadProgress(closure: {
                    progress in

                    print(progress.fractionCompleted)
                })
            case .failure(let encodingError):
                
                print(encodingError)
            }
        }
    }
}
