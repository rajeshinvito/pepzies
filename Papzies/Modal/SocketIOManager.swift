//
//  SocketIOManager.swift
//  Crete-Link
//
//  Created by osx on 28/09/18.
//  Copyright © 2018 AppsMaven. All rights reserved.
//
//


import UIKit
import SocketIO
import SystemConfiguration
var socketConnected = false

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    let manager = SocketManager(socketURL: URL(string: "http://3.136.221.59:3000")!, config: [.log(true), .compress])
    var socket:SocketIOClient!
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func establishConnection() {
       
        socket.connect()
        socket.on(clientEvent: .connect) {data, ack in
            print("socket IDD \(self.socket.sid)")
            socketConnected = true
            print("socket connected")
            let sessionStatus = UserDefaults.standard.bool(forKey: UserConstant.Session_Status)
            if sessionStatus{
                let sessionId = UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? ""
                 let role = UserDefaults.standard.string(forKey: UserConstant.user_type)
                SocketIOManager.sharedInstance.checkSession(sessionId: sessionId, type: role!)

            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SocketConnects"), object: "noty touched")
        }
    }
    
    func closeConnection() {
        socketConnected = false
        print("socket disconnected")
        socket.disconnect()
    }
    
    func CloseAllEvent(){
        socket.off("getMessagessingleResponse")
    }
    
    
    func callOfAnyEvent(_ senderId: String, completionHandler: @escaping (_ userList: [[String: AnyObject]]?) -> Void) {
        socket.onAny {
            print("Got event: \($0.event), with items: \($0.items!)")
            
        }
    }

    func HirePapzies(id:String,receiver_id: String, estimated_time: String, latitude: String, longitude: String, distance: String) {
        
        socket.emit("createSession", ["receiver_id": receiver_id, "estimated_time": estimated_time,  "latitude": latitude, "longitude": longitude, "distance": distance,"id":id])
        
    }
    
    func getRequestFromUser(id:String, _ completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("\(id)accept-decline-session") { (dataArray, socketAck) -> Void in
            
            let dataArr = dataArray[0] as! NSDictionary
            print(dataArr)
            completionHandler(dataArr as! [String : AnyObject])
        }
    }
    
    func acceptORDeclineNewRequest(session_id:String,status:String){
        socket.emit("accDecSession", ["session_id": session_id, "status": status])

    }
    
    func getAcceptandDeclineReponse(id:String, _ completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("\(id)accept-decline-response") { (dataArray, socketAck) -> Void in
            
            let dataArr = dataArray[0] as! NSDictionary
            print(dataArr)
            completionHandler(dataArr as! [String : AnyObject])
        }
    }
    
    func Joinchatroom(sessionId:String, _ completionHandler: @escaping (_ messageInfo: [Any]) -> Void) {
        socket.emit("joinRoom", ["sessionId": sessionId])
        socket.emit("getMessages", ["roomId": sessionId])
        socket.on(sessionId) { (dataArray, socketAck) -> Void in
            // let dataArr = dataArray[0] as! [Any]
            //print(dataArr)
            completionHandler(dataArray as! [Any])
        }
    }
    func sendmessage(roomId:String,type:String,message : String){
           socket.emit("message", ["roomId": roomId, "type": type,"messageText":message])
          }
    
    func updatePaziesLocation(latitude:String,longitude:String,id:String,sessionId:String){
        socket.emit("updatePapziLocation", ["latitude": latitude, "longitude": longitude,"id":id,"sessionId":sessionId])

    }
    
    func getCurrentLocationOFPazies(sessionId:String, _ completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void){
        
        socket.on("\(sessionId)updatePapziLocation") { (dataArray, socketAck) -> Void in
            
            let dataArr = dataArray[0] as! NSDictionary
            print(dataArr)
            completionHandler(dataArr as! [String : AnyObject])
        }

    }
    
    func checkSession(sessionId:String,type:String){
        socket.emit("checkSession", ["sessionId": sessionId, "type": type])
    }
    
   
    
    func listnerForCheckSession(sessionId:String,type:String, _ completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void){
        
        socket.on("\(sessionId)getSession\(type)") { (dataArray, socketAck) -> Void in
            
            let dataArr = dataArray[0] as! NSDictionary
            print(dataArr)
            completionHandler(dataArr as! [String : AnyObject])
        }
    }

    func cancelAcceptedSession(sessionId:String){
        socket.emit("cancelSession", ["sessionId": sessionId])

    }


        
}






