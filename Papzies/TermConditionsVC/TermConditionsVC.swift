//
//  TermConditionsVC.swift
//  Papzies
//
//  Created by apple on 11/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TermConditionsVC: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cmdAgree(_ sender: Any) {
        acceptTermandCondition()

    }
    
    func acceptTermandCondition(){

        let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
        let param:[String:Any] = ["status":"1"]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "private/accept-tc", parameters: param, bearerToken: "Bearer \(BearerToken)"){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            
                            let role = UserDefaults.standard.string(forKey: UserConstant.user_type)!
                            if role == "1"{
                                let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SafetyCheckVC") as! SafetyCheckVC
                                self.navigationController?.pushViewController(Vc, animated: true)

                            }else{
                                let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SafetyCheckVC") as! SafetyCheckVC
                                self.navigationController?.pushViewController(Vc, animated: true)

                            }
                            
                        }
                    }
                }
            }
        }
    }
}
