//
//  ChangePasswordVC.swift
//  Papzies
//
//  Created by apple on 17/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ChangePasswordVC: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cmdChange(_ sender: Any) {
        self.textFieldValidation()
    }
    @IBAction func cdmForgotPassword(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(Vc, animated: true)

    }
    
    func textFieldValidation(){
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            if txtField.isEmpty{
                self.Alert(title: "Alert!", message: "Please enter password")
                return
            }
        }
        if txtNewPassword.text != txtConfirmPassword.text{
            self.Alert(title: "Alert!", message: "New and confirm password do not match")
            return
        }
        self.changepassword()
    }

    
    func changepassword(){
        
         let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
        let param:[String:Any] = ["oldPassword":txtOldPassword.text!,"newPassword":txtNewPassword.text!]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "private/change-password", parameters: param, bearerToken: "Bearer \(BearerToken)"){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }

    }
}
