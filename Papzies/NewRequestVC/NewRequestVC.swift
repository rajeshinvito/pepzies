//
//  NewRequestVC.swift
//  Papzies
//
//  Created by apple on 28/01/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

protocol NewRequestAccepted {
    func PaziesAcceptedNewRequest(pazies:[String:Any])
}

class NewRequestVC: UIViewController {

    @IBOutlet weak var btnDeclineRequest: UIButton!
    @IBOutlet weak var btnAcceptRequest: UIButton!
    @IBOutlet weak var requestEstTime: UILabel!
    @IBOutlet weak var requestEstCost: UILabel!
    @IBOutlet weak var requestFromPlace: UILabel!
    @IBOutlet weak var requestFromDistance: UILabel!
    @IBOutlet weak var requestFromRating: UILabel!
    @IBOutlet weak var requestFromName: UILabel!
    @IBOutlet weak var requestFromImage: UIImageView!
    var pepziesDetails:[String:Any]?
    var delegate:NewRequestAccepted?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRippleEffect(to: btnAcceptRequest)
        requestEstTime.text = "\(pepziesDetails?["estimate_time"] as? String ?? "") min"
        requestEstCost.text = "$\(pepziesDetails?["estimate_fare"] as? Int ?? 0)"
        requestFromPlace.text = pepziesDetails?["address"] as? String ?? ""
        requestFromDistance.text = "\(pepziesDetails?["distance"] as? String ?? "") mi"
        requestFromRating.text = pepziesDetails?["rating"] as? String ?? ""
        requestFromName.text = pepziesDetails?["name"] as? String ?? ""
        requestFromImage.sd_setImage(with: URL(string: pepziesDetails?["image"] as? String ?? ""), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cmdAcceptRequest(_ sender: Any) {
        acceptOrDeclineNewRequest(session_id: pepziesDetails?["sessionId"] as? String ?? "", status: "1")
        self.delegate?.PaziesAcceptedNewRequest(pazies: pepziesDetails!)
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func cmdDeclineRequest(_ sender: Any) {
        acceptOrDeclineNewRequest(session_id: pepziesDetails?["sessionId"] as? String ?? "", status: "0")
         self.dismiss(animated: true, completion: nil)
    }
    //
    


    func acceptOrDeclineNewRequest(session_id:String,status:String){
        
        SocketIOManager.sharedInstance.acceptORDeclineNewRequest(session_id: session_id, status: status)
    }
    
    
}

