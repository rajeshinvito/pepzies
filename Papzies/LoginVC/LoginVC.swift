//
//  LoginVC.swift
//  Papzies
//
//  Created by apple on 09/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMaps

class LoginVC: UIViewController,NVActivityIndicatorViewable,LocationUpdateProtocol {
    
    
    @IBOutlet weak var btnCreateAccount: UIButton!
    @IBOutlet weak var btnLoginWithFacebook: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPassword: TextField!
    @IBOutlet weak var txtEmail: TextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        let locationServerices = UserLocationManager.SharedManager
        locationServerices.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    @IBAction func cmdLogin(_ sender: Any) {
        
        self.view.endEditing(true)
        textFieldValidation()
    }
    @IBAction func cmdForgotPassword(_ sender: Any) {//
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        Vc.fromLogin = true
        self.navigationController?.pushViewController(Vc, animated: true)
        
    }
    @IBAction func cmdLoginWithFacebook(_ sender: Any) {
    }
    @IBAction func cmdCreateAccount(_ sender: Any) {
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChooseRoleVC") as! ChooseRoleVC
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    func textFieldValidation(){
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            if txtField.isEmpty{
                self.Alert(title: "Alert!", message: "Please enter \(txtField.placeholder!)")
                return
            }
        }
        self.userSignIn()
    }
    func locationDidUpdateToLocation(location: CLLocation) {
        
        UserDefaults.standard.set(location.coordinate.latitude, forKey: "Lat")
        UserDefaults.standard.set(location.coordinate.latitude , forKey: "Long")
    }
    
    
    func userSignIn(){
        
        var DeviceToken = "asdfgajsfgaksfgasfgaksfafaskf"
        if (UserDefaults.standard.string(forKey: "DeviceToken") != nil){
            DeviceToken = UserDefaults.standard.string(forKey: "DeviceToken")!
        }
        let UDID = UIDevice.current.identifierForVendor?.uuidString
        let param:[String:Any] = ["email":txtEmail.text!,"password":txtPassword.text!,"device_token":DeviceToken,"device_id":UDID!,"device_platform":"IOS"]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "login", parameters: param, bearerToken: ""){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            if let payload = data["payload"] as? Dictionary<String,Any>{
                                
                                let bearerToken = payload["token"] as! String
                                UserDefaults.standard.set(bearerToken, forKey: UserConstant.bearer_Token)
                                
                                let type = payload["type"] as! Int
                                UserDefaults.standard.set("\(type)", forKey:UserConstant.user_type )
                                
                                let userId = payload["id"] as! String
                                UserDefaults.standard.set(userId, forKey: UserConstant.user_Id)
                                SocketIOManager.sharedInstance.establishConnection()
                                
                                let rating = payload["rating"] as! String
                                UserDefaults.standard.set("\(rating)", forKey:UserConstant.User_Rating )
                                
                                let bio = payload["bio"] as? String ?? ""
                                UserDefaults.standard.set(bio, forKey: "Bio")
                                
                                let userName = "\(payload["fname"] as! String) \(payload["lname"] as! String)"
                                UserDefaults.standard.set(userName, forKey: UserConstant.name)
                                
                                let userImage = payload["image"] as! String
                                UserDefaults.standard.set(userImage, forKey: UserConstant.profile_pic)
                                
                                let numberverified = payload["numberverified"] as! String
                                UserDefaults.standard.set(numberverified, forKey: "numberverified")
                                
                                let emailverified = payload["emailverified"] as! String
                                UserDefaults.standard.set(emailverified, forKey: "emailverified")
                                
                                let termCondition = payload["tc"] as! String
                                UserDefaults.standard.set(termCondition, forKey: "termCondition")
                                
                                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: payload)
                                UserDefaults.standard.set(encodedData, forKey: "UserData")
                                
                                DispatchQueue.main.async {
                                    if numberverified == "0"{
                                        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyMobilesVC") as! VerifyMobilesVC
                                        Vc.mobileNumber = payload["mobile"] as! String
                                        self.navigationController?.pushViewController(Vc, animated: true)
                                    }else if emailverified == "0"{
                                        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyEmailVC") as! VerifyEmailVC
                                        Vc.emailAddress = payload["email"] as! String
                                        self.navigationController?.pushViewController(Vc, animated: true)
                                    }else if bio == ""{
                                        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PapziesDescriptionVC") as! PapziesDescriptionVC
                                        self.navigationController?.pushViewController(Vc, animated: true)
                                    }else if userImage == "" {
                                        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPictureVC") as! SelectPictureVC
                                        self.navigationController?.pushViewController(Vc, animated: true)
                                    }else if termCondition == "0"{
                                        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "TermConditionsVC") as! TermConditionsVC
                                        self.navigationController?.pushViewController(Vc, animated: true)
                                    }else{
                                        let Storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let Vc = Storyboard.instantiateViewController(withIdentifier: "LetsStartVC") as! LetsStartVC
                                        self.navigationController?.pushViewController(Vc, animated: true)
                                    }
                                }
                            }else{
                                
                            }
                        }
                    }else{
                        let message = data["message"] as! String
                        self.Alert(title: "Alert!", message: message)
                    }
                }
                
            }else{
                self.Alert(title: "Alert!", message: "Something went wrong. Please try again later!")
                self.stopAnimating()
            }
        }
    }
    
}
