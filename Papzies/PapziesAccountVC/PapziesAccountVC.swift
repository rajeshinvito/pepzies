//
//  PapziesAccountVC.swift
//  Papzies
//
//  Created by apple on 17/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SDWebImage

class PapziesAccountVC: UIViewController {

    
    @IBOutlet weak var txtDisc: UITextView!
    @IBOutlet weak var txtMobileNumber: UIKerningTextField!
    @IBOutlet weak var txtCountry: UIKerningTextField!
    @IBOutlet weak var txtZipCode: UIKerningTextField!
    @IBOutlet weak var txtState: UIKerningTextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtLastName: UIKerningTextField!
    @IBOutlet weak var txtFirstName: UIKerningTextField!
    @IBOutlet weak var txtEmail: UIKerningTextField!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    var userDeatils:[String:Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        btnCamera.isHidden = true
        txtDisc.isUserInteractionEnabled = false
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            txtField.isUserInteractionEnabled = false
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "UserData") != nil{
            let decoded = preferences.object(forKey: "UserData")  as! Data
            userDeatils = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String:Any]
            
            print(userDeatils)
            if userDeatils["image"] as? String == "" {
                let Image = UserDefaults.standard.string(forKey: UserConstant.profile_pic)!
                self.userImage.sd_setImage(with: URL(string: Image), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)

            }else{
                userImage.sd_setImage(with: URL(string: userDeatils["image"] as? String ?? ""), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
            }
            txtEmail.text = "\(userDeatils["email"] as? String ?? "")"
            txtFirstName.text = "\(userDeatils["fname"] as? String ?? "")"
            txtLastName.text = "\(userDeatils["lname"] as? String ?? "")"
            txtAddress.text = "\(userDeatils["address"] as? String ?? "")"
            if userDeatils["bio"] as? String == "" {
                txtDisc.text = UserDefaults.standard.string(forKey: "Bio")
            }else{
                 txtDisc.text = "\(userDeatils["bio"] as? String ?? "")"
            }
            txtState.text = "\(userDeatils["state"] as? String ?? "")"
            txtZipCode.text = "\(userDeatils["zip"] as? String ?? "")"
            txtCountry.text =  "\(userDeatils["country"] as? String ?? "")"
            txtMobileNumber.text =  "\(userDeatils["mobile"] as? String ?? "")"
            
        } else {
            userDeatils = [String:Any]()
        }

    }
    
    
    @IBAction func cmdMenu(_ sender: Any) {
        
        let role = UserDefaults.standard.string(forKey: UserConstant.user_type)
        if role == "1"{
            UsersharedMenuClass.menuObj.addMenuClass(view: self.view)
            UsersharedMenuClass.menuObj.menuView.Userdelegate = self

        }else{
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self

        }
    }
    
    @IBAction func cmdCamera(_ sender: Any) {
    }
    @IBAction func cmdEdit(_ sender: Any) {
        btnCamera.isHidden = false
        txtDisc.isUserInteractionEnabled = true
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            txtField.isUserInteractionEnabled = true
        }

        
    }
    @IBAction func cmdChangePassword(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
}

extension PapziesAccountVC:UserMenuDelegate {
    
    func UsercallMethod1(vcIndex: Int) {
        UsernavigationClass1(index:vcIndex)
    }
    func UserMenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func UserscreenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            UsersharedMenuClass.menuObj.addMenuClass(view: self.view)
            UsersharedMenuClass.menuObj.menuView.Userdelegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func UsercallMethod(vcIndex:Int) {
        UsernavigationClass(index:vcIndex)
    }

    func UsertouchEvents(_ direction:String) {
        if direction == "left" {
            UsersharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            UsersharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

extension PapziesAccountVC:MenuDelegate {
    func callMethod1(vcIndex: Int) {
        navigationClass1(index:vcIndex)
    }
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }

    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}


