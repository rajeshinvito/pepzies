//
//  MenuCustCell.swift
//  JalfApp
//
//  Created by osx on 01/01/18.
//  Copyright © 2018 osx. All rights reserved.
//

import UIKit

class MenuCustCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!

    @IBOutlet weak var buttonTapOut: UIButton!
    @IBOutlet weak var imageLeading: NSLayoutConstraint!
    @IBOutlet weak var icon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var downArrowImage: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
