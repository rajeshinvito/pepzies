//
//  MenuClassView.swift
//  JalfApp
//
//  Created by osx on 29/12/17.
//  Copyright © 2017 osx. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol MenuDelegate {
    func callMethod1(vcIndex:Int)
    func callMethod(vcIndex:Int)
    func  touchEvents(_ direction:String)
}

let kNotificationMenuChanged = NSNotification.Name(rawValue:"kNotificationMenuChanged")
class MenuClassView: UIView,UITableViewDelegate,UITableViewDataSource{
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    let kHeaderSectionTag: Int = 6900;
    var collpseSection :Int = -1
    var delegate : MenuDelegate? = nil
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var rightButtonOut: UIButton!
    @IBOutlet var customeView :UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var tableView2: UITableView!
    
    var subList = ["Block List","My Profile","Change Settings"]
    var menuClass = [MenuClass]()
    var VcInfo:[String] = []
    var VcIcons:[String] = []
    
    @IBOutlet weak var btnNotificationBadage: MFBadgeButton!
    var menuClass2 = [MenuClass]()
    var VcInfo2:[String] = []
    var VcIcons2:[String] = []

    @IBOutlet weak var lblUserMobile: UILabel!
    @IBOutlet weak var userId: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBAction func cmdNotificationBadage(_ sender: Any) {
        
    }
    func initializingForMenu() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveMenuChangedNotification), name: kNotificationMenuChanged, object: nil)
            VcIcons = ["Home","Account","Documents","Wallet","Earnings","Payments","Sessions"]
        VcInfo = ["Home","Account","Documents","Wallet","Earnings","Payments","Session"]
            menuClass.removeAll()
            menuClass.append(MenuClass(sectionName: VcInfo[0], rowsInSections: [],selected:false))
            menuClass.append(MenuClass(sectionName: VcInfo[1], rowsInSections: [],selected:false))
            menuClass.append(MenuClass(sectionName: VcInfo[2], rowsInSections: [],selected:false))
            menuClass.append(MenuClass(sectionName: VcInfo[3], rowsInSections: [],selected:false))
            menuClass.append(MenuClass(sectionName: VcInfo[4], rowsInSections: [],selected:false))
            menuClass.append(MenuClass(sectionName: VcInfo[5], rowsInSections: [],selected:false))
            menuClass.append(MenuClass(sectionName: VcInfo[6], rowsInSections: [],selected:false))
        
        VcIcons2 = ["About","Help","LogOut"]
        VcInfo2 = ["About","Help","Logout"]
        menuClass2.removeAll()
        menuClass2.append(MenuClass(sectionName: VcInfo2[0], rowsInSections: [],selected:false))
        menuClass2.append(MenuClass(sectionName: VcInfo2[1], rowsInSections: [],selected:false))
        menuClass2.append(MenuClass(sectionName: VcInfo2[2], rowsInSections: [],selected:false))
        print("ranga said for print 1")
        
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        initializingForMenu()
         print("ranga said for print 2")
//        btnNotificationBadage.setImage(UIImage(named: "notification_nav bar")?.withRenderingMode(.alwaysTemplate), for: .normal)
//        btnNotificationBadage.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
//        btnNotificationBadage.badge = "20"


        Bundle.main.loadNibNamed("menu", owner: self, options: nil)
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width*0.8, height: UIScreen.main.bounds.height)
        customeView.frame = self.frame
        addSubview(customeView)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        
//        profileImage.image = UIImage(named:"nobody")
        self.customeView.addGestureRecognizer(swipeLeft)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MenuCellX", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        
        tableView2.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView2.delegate = self
        tableView2.dataSource = self
        tableView2.register(UINib(nibName: "MenuCellX", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        
        rightButtonOut.addTarget(self, action: #selector(buttonTouched), for: .touchUpInside)

        let Image = UserDefaults.standard.string(forKey: UserConstant.profile_pic)!
        self.userImage.sd_setImage(with: URL(string: Image), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
        self.userName.text = UserDefaults.standard.string(forKey: UserConstant.name)!
        

    }
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        switch gesture.direction {
        case UISwipeGestureRecognizer.Direction.right:
            delegate?.touchEvents("right")
        case UISwipeGestureRecognizer.Direction.down:
            delegate?.touchEvents("down")
        case UISwipeGestureRecognizer.Direction.left:
            delegate?.touchEvents("left")
        case UISwipeGestureRecognizer.Direction.up:
            delegate?.touchEvents("up")
        default:
            break
        }
    }
    
    @objc func buttonTouched(sender:UIButton) {
        delegate?.touchEvents("right")
        
    }
    @objc func receiveMenuChangedNotification(notification:NSNotification) {
        
        if notification.name == kNotificationMenuChanged {
             print("ranga said for print 3")
            initializingForMenu()
            tableView.reloadData()
            tableView2.reloadData()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if collpseSection == section && menuClass[section].selected{
            return menuClass[section].rowsInSections.count
            
        }
        else {
            return 0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableView2{
            return menuClass2.count
        }
        return menuClass.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if UIDevice.current.userInterfaceIdiom == .phone{
            
            return tableView.frame.height/9
        }
        return tableView.frame.height / 6

       
       
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
        return 45
        
    }
    @objc func sectionHeaderWasTouched(_ sender: UIButton) {
        
        guard let tag = sender.tag as? Int  else {
            return
        }
        print(tag)
//        if tag != 5 {
            // navigate to the screen
            delegate?.callMethod(vcIndex:tag)
//        }
        menuClass[tag].selected = !menuClass[tag].selected
        collpseSection = tag
        self.tableView.reloadData()
        
    }
        @objc func sectionHeaderWasTouched1(_ sender: UIButton) {
            
            guard let tag = sender.tag as? Int  else {
                return
            }
            print(tag)
    //        if tag != 5 {
                // navigate to the screen
            delegate?.callMethod1(vcIndex:tag)
    //        }
            menuClass2[tag].selected = !menuClass2[tag].selected
            collpseSection = tag
            self.tableView2.reloadData()
            
        }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuCustCell
        if tableView == tableView2{
            cell.label.text = menuClass2[section].sectionName
            cell.backgroundColor = UIColor.clear
            cell.imageLeading.constant = 15
            cell.tag = section
            cell.buttonTapOut.tag = section
            if section == 0{
            
            }
            
            if section == 1{
                
                            }
            cell.buttonTapOut.addTarget(self, action: #selector(sectionHeaderWasTouched1), for: .touchUpInside)

            if  menuClass2[section].rowsInSections.count == 0 {
                cell.downArrowImage.isHidden = true
                
            }
            else {
                cell.downArrowImage.isHidden = false
            }
            cell.icon.image = UIImage(named: VcIcons2[section])
            return cell.contentView

        }else{
            cell.label.text = menuClass[section].sectionName
            cell.backgroundColor = UIColor.clear
            cell.imageLeading.constant = 15
            cell.tag = section
            cell.buttonTapOut.tag = section
            if section == 0{
            
            }
            
            if section == 1{
                
                            }
            cell.buttonTapOut.addTarget(self, action: #selector(sectionHeaderWasTouched), for: .touchUpInside)

            if  menuClass[section].rowsInSections.count == 0 {
                cell.downArrowImage.isHidden = true
                
            }
            else {
                cell.downArrowImage.isHidden = false
            }
            cell.icon.image = UIImage(named: VcIcons[section])
            return cell.contentView

        }
        
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tableView2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCustCell
            cell.selectionStyle = .none
            cell.imageLeading.constant = 60
            cell.downArrowImage.isHidden = true
            cell.label.text = menuClass2[indexPath.section].rowsInSections[indexPath.row]
            cell.contentView.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
            
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCustCell
            cell.selectionStyle = .none
            cell.imageLeading.constant = 60
            cell.downArrowImage.isHidden = true
            cell.label.text = menuClass[indexPath.section].rowsInSections[indexPath.row]
            cell.contentView.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
            
            return cell

        }
        
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableView2{
            if indexPath.row == 0 ||  indexPath.row == 1 ||  indexPath.row == 2 || indexPath.row ==  3 || indexPath.row == 5
            {
                delegate?.callMethod1(vcIndex:indexPath.row)
            }
            else {
            }

        }else{
            if indexPath.row == 0 ||  indexPath.row == 1 ||  indexPath.row == 2 || indexPath.row ==  3 || indexPath.row == 5
            {
                delegate?.callMethod(vcIndex:indexPath.row)
            }
            else {
            }
        }
    }
}

extension UIViewController {
    func navigationClass(index:Int) {
        sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        let Storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        var Vc = UIViewController()
                switch index {
                case 0:
                    Vc = Storyboard.instantiateViewController(withIdentifier: "PapziesHomeVC") as! PapziesHomeVC
                case 1:
                    Vc = Storyboard.instantiateViewController(withIdentifier: "PapziesAccountVC") as! PapziesAccountVC
                case 2:
                    Vc = Storyboard.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
                case 3:
                    Vc = Storyboard.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
                case 4:
                    Vc = Storyboard.instantiateViewController(withIdentifier: "LetsStartVC") as! LetsStartVC
                case 5:
                    Vc = Storyboard.instantiateViewController(withIdentifier: "LetsStartVC") as! LetsStartVC

                default:
                    print("case 0")
                }
                var vcArray = self.navigationController?.viewControllers
                vcArray?.removeLast()
                vcArray?.append(Vc)
                DispatchQueue.main.async {
                    self.navigationController?.setViewControllers(vcArray!, animated: true)
                    
                }
    }
    
        func navigationClass1(index:Int) {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
            
            var Vc = UIViewController()
                if index == 2{
                    let alert = UIAlertController(title: "Logout?", message: "Are your sure want to logout?",  preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.destructive, handler: { _ in
                        //Cancel Action
                    }))
                    alert.addAction(UIAlertAction(title: "YES",
                                                  style: UIAlertAction.Style.default,
                                                  handler: {(_: UIAlertAction!) in
                                                    self.logout()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                    
                else{
                    switch index {
                        
                    case 0:
                        Vc = self.storyboard?.instantiateViewController(withIdentifier: "LetsStartVC") as! LetsStartVC
                    case 1:
                        Vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC

                    default:
                        print("case 6")
                    }
                    var vcArray = self.navigationController?.viewControllers
                    vcArray?.removeLast()
                    vcArray?.append(Vc)
                    DispatchQueue.main.async {
                        self.navigationController?.setViewControllers(vcArray!, animated: true)}
        }
    }
                                    
    func logout(){
        DispatchQueue.main.async {

        UserDefaults.standard.set(false, forKey: "LoggedIn")
            UserDefaults.standard.set(false, forKey: UserConstant.login_Status)
            UserDefaults.standard.synchronize()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let Storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let login = Storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigationController = UINavigationController(rootViewController: login)
        navigationController.navigationBar.isTranslucent = false
        appDelegate.window?.rootViewController = navigationController
        appDelegate.window?.makeKeyAndVisible()
            
            let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
            APIClass.apiClass.PostRequest2(api: "private/logout", parameters: [:], bearerToken: "Bearer \(BearerToken)"){(response, status) in
                if status{
                    if let data = response as? Dictionary<String,Any>{
                        print(data)
                        if let status = data["status"] as? Int{
                            if status == 0{
                                let message = data["message"] as! String
                                self.Alert(title: "Alert!", message: message)
                            }else{
                                if let payload = data["payload"] as? Dictionary<String,Any>{
//                                    if let appDomain = Bundle.main.bundleIdentifier {
//                                        UserDefaults.standard.removePersistentDomain(forName: appDomain)
//                                        UserDefaults.standard.synchronize()
//                                        UserLocationManager.SharedManager.stopLocation()
//                                        UserLocationManager.SharedManager.StartLocation()
//
//                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
                
    }

class MenuClass {
    
    var sectionName = String()
    var rowsInSections = [String]()
    var selected = Bool()

    init(sectionName:String,rowsInSections:[String],selected:Bool) {
        self.sectionName = sectionName
        self.rowsInSections = rowsInSections
        self.selected = selected
    }
}
