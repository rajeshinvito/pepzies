//
//  PapziesSignupVC.swift
//  Papzies
//
//  Created by apple on 10/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CountryPickerView

class PapziesSignupVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var txtConfirmPassword: UIKerningTextField!
    @IBOutlet weak var txtPassword: UIKerningTextField!
    //@IBOutlet weak var txtMobileNumber: UIKerningTextField!
    @IBOutlet weak var txtMobileNumber: UITextField!

    @IBOutlet weak var txtCountry: UIKerningTextField!
    @IBOutlet weak var txtZipCode: UIKerningTextField!
    @IBOutlet weak var txtState: UIKerningTextField!
    @IBOutlet weak var txtCity: UIKerningTextField!
    @IBOutlet weak var txtAddress: UIKerningTextField!
    @IBOutlet weak var txtEmail: UIKerningTextField!
    @IBOutlet weak var txtLastName: UIKerningTextField!
    @IBOutlet weak var txtFirstName: UIKerningTextField!
    @IBOutlet weak var btnBack: UIButton!
    var user_Type = ""
    var countryCode = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 100  , height: txtMobileNumber.frame.size.height))
//        cp.countryDetailsLabel.frame.origin.x = -10
        cp.flagImageView.isHidden = false
        cp.isUserInteractionEnabled = true
        txtMobileNumber.leftView = cp
        txtMobileNumber.leftViewMode = .always
        cp.textColor = .black
        //        cp.font = UIFont(name: "ApercuRegular", size: 16.0)!
        countryCode = cp.selectedCountry.phoneCode
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.stopAnimating(nil)
    }
    
    @IBAction func cmdContinue(_ sender: Any) {
        self.view.endEditing(true)
        self.textFieldValidation()
    }
    
    func textFieldValidation(){
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            if txtField.isEmpty{
                self.Alert(title: "Alert!", message: "Please enter \(txtField.placeholder!)")
                return
            }
        }
        if !txtEmail.isValidEmail(){
            self.Alert(title: "Alert!", message: "Please enter valid email address")
        }else if txtPassword.text != txtConfirmPassword.text{
            self.Alert(title: "Alert!", message: "Password and confirm password don't match")
        }else {
            self.userSignUp()
        }
    }

    func userSignUp(){
        
        var DeviceToken = ""
        if (UserDefaults.standard.string(forKey: "DeviceToken") != nil){
            DeviceToken = UserDefaults.standard.string(forKey: "DeviceToken")!
        }
        let UDID = UIDevice.current.identifierForVendor?.uuidString
        let param:[String:Any] = ["fname":txtFirstName.text!,"lname":txtLastName.text!,"email":txtEmail.text!,"address":txtAddress.text!,"city":txtCity.text!,"state":txtState.text!,"zip":txtZipCode.text!,"country":txtCountry.text!,"mobile":"\(countryCode)\(txtMobileNumber.text!)","password":txtPassword.text!,"device_token":DeviceToken,"device_id":UDID!,"device_platform":"IOS","u_type":self.user_Type]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "signup", parameters: param, bearerToken: ""){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            if let payload = data["payload"] as? Dictionary<String,Any>{
                                let bearerToken = payload["token"] as! String
                                UserDefaults.standard.set(bearerToken, forKey: UserConstant.bearer_Token)
                                let userId = payload["id"] as! String
                                UserDefaults.standard.set(userId, forKey: UserConstant.user_Id)
                                let type = payload["type"] as! Int
                                UserDefaults.standard.set(type, forKey: UserConstant.user_type)
                                let userName = "\(payload["fname"] as! String) \(payload["lname"] as! String)"
                                UserDefaults.standard.set(userName, forKey: UserConstant.name)
                                let numberverified = payload["numberverified"] as! String
                                UserDefaults.standard.set(numberverified, forKey: "numberverified")
                                
                                let emailverified = payload["emailverified"] as! String
                                UserDefaults.standard.set(emailverified, forKey: "emailverified")
                                
                                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: payload)
                                UserDefaults.standard.set(encodedData, forKey: "UserData")
                                
                                DispatchQueue.main.async {
//                                    UserDefaults.standard.set(true, forKey: "LoggedIn")
//                                    UserDefaults.standard.synchronize()
                                let Vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyMobilesVC") as! VerifyMobilesVC
                                    Vc.mobileNumber = payload["mobile"] as! String
                                self.navigationController?.pushViewController(Vc, animated: true)

                                }
                            }else{
                                
                            }
                        }
                    }else{
                        let message = data["message"] as! String
                        self.Alert(title: "Alert!", message: message)
                    }
                }
            }else{
                 self.stopAnimating()
            }
        }
    }
}

extension PapziesSignupVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string).replacingOccurrences(of: " ", with: " ")
        if text == " "{
            return false
        }
        if textField == txtMobileNumber{
            if text.count > 15  {
                return false
            }
        }
        if textField == txtZipCode{
            if text.count > 10  {
                return false
            }
        }

        return true
    }
}
