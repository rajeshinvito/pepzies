//
//  AddCardVC.swift
//  Papzies
//
//  Created by apple on 11/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class AddCardVC: UIViewController {

    @IBOutlet weak var btnAddCard: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()


    }

    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cmdAddCard(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "LetsStartVC") as! LetsStartVC
        self.navigationController?.pushViewController(Vc, animated: true)


    }
    
}
