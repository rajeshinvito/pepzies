//
//  SafetyCheckVC.swift
//  Papzies
//
//  Created by apple on 11/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class SafetyCheckVC: UIViewController {

    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cmdContinue(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "AddCardVC") as! AddCardVC
        self.navigationController?.pushViewController(Vc, animated: true)

    }
    
    
}
