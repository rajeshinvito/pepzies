//
//  UserDashboardVC.swift
//  Papzies
//
//  Created by apple on 13/01/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
struct chat_model {
    let  msg : String?
    let  type : String!
    init(_ Dict : Dictionary<String,Any>){
        self.msg = Dict["msg"] as? String ?? ""
        self.type = "\(Dict["type"]!)"
    }
}


import CoreLocation
import GoogleMaps
import GooglePlaces
import NVActivityIndicatorView
import AnimatedCollectionViewLayout
class UserDashboardVC: UIViewController,HirePapzies,NVActivityIndicatorViewable {
    
    //MARK:-   hired session view
    @IBOutlet weak var finalPaziesDetailsView: UIView!
    @IBOutlet weak var finalPaziesDeatilViewWithBackButton: UIView!
    @IBOutlet weak var hiredPapziesVIew: UIView!
    @IBOutlet weak var heightOfHiredPaziesVIew: NSLayoutConstraint!
    @IBOutlet weak var bottomConstantOfHiredPapziesView: NSLayoutConstraint!
    @IBOutlet weak var heightOfCardView: NSLayoutConstraint!
    @IBOutlet weak var cardDetailView: UIView!
    @IBOutlet weak var LocationView: UIView!
    @IBOutlet weak var startButtonView: UIView!
    @IBOutlet weak var sendMessageView: UIView!
    
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var txtSendMessage: UITextField!
    @IBOutlet weak var txtMessage: UIView!
    @IBOutlet weak var btnFinalStartSession: UIButton!
    @IBOutlet weak var lblFinalLocationTo: UILabel!
    @IBOutlet weak var lblFinalLocationFrom: UILabel!
    @IBOutlet weak var distanceOfFinalPapzies: UILabel!
    @IBOutlet weak var finalPapziesCardNumber: UILabel!
    @IBOutlet weak var finalPaziesCardType: UIImageView!
    @IBOutlet weak var btnFinalPaziesCall: UIButton!
    @IBOutlet weak var btnFinalPaziesOpenChat: UIButton!
    @IBOutlet weak var finalPaziesTag: UILabel!
    @IBOutlet weak var finalPapziesName: UILabel!
    @IBOutlet weak var finalPapziesImage: UIImageView!
    @IBOutlet weak var finalPapziesName2: UILabel!
    
    
    //MARK:-   Wating for papzies outlets
    @IBOutlet weak var waitingForPapziesView: UIView!
    @IBOutlet weak var waitingForPapziesImage: UIImageView!
    @IBOutlet weak var waitingForPapziesName: UILabel!
    @IBOutlet weak var waitingForPaziesRating: UILabel!
    @IBOutlet weak var btnCancelWaitingPapzies: UIButton!
    @IBOutlet weak var lblForWatingPapzies: UILabel!
    @IBOutlet weak var bottomConstantForWatingPapziesView: NSLayoutConstraint!
    
    //MARK:-   Hiring for papzies
    var hiringPaziesLat = ""
    var hiringPaziesLong = ""
    
    //MARK:-   Estimated Total Price View  outlets
    @IBOutlet weak var estimatesTotalPriceView: UIView!
    @IBOutlet weak var estimatedTotalPriceBottomConst: NSLayoutConstraint!
    @IBOutlet weak var lblDistanceFare: UILabel!
    @IBOutlet weak var lblPapziesFare: UILabel!
    @IBOutlet weak var lblTotalSessionTime: UILabel!
    @IBOutlet weak var lblTotalEstimatedFare: UILabel!
    @IBOutlet weak var btnBackEstimateTotalFare: UIButton!
    @IBOutlet weak var btnContinueFromTotalEstimateFare: UIButton!
    
    
    //MARK:-   Search NearBy adn Estimate PriceView outlets
    
    @IBOutlet weak var lableNearBySearchView: UILabel!
    @IBOutlet weak var ImageNearBySearch: UIImageView!
    @IBOutlet weak var bottomConstantOfNearByPapziesView: NSLayoutConstraint!
    @IBOutlet weak var searchNearByPapziesView: UIView!
    
    
    //MARK:-  Near BY user CollectionView
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewBottomConstant: NSLayoutConstraint!
    
    //MARK:-   searching Near By Papzies
    
    
    //MARK:-  Estimate Time Session View
    var EstimateTime = ""
    @IBOutlet weak var estimateTimeViewConstant: NSLayoutConstraint!
    @IBOutlet weak var estimateTimeView: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnAbort: UIButton!
    @IBOutlet weak var txtEnterTime: UITextField!
    
    //MARK:-   Maps Related outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var txtSearchedName: UIKerningTextField!
    @IBOutlet weak var searchLocationNameView: UIView!
    @IBOutlet weak var btnSearchLocation: UIButton!
    @IBOutlet weak var locationSearchBottomConstant: NSLayoutConstraint!
    @IBOutlet weak var btnHideLocationSearchView: UIButton!
    @IBOutlet weak var txtLocation: UIView!
    @IBOutlet weak var locationSearchView: UIView!
    @IBOutlet weak var btnSlideToHire: UIButton!
    @IBOutlet weak var slideView: UIView!
    @IBOutlet weak var btnUserImage: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var btnMenu: UIButton!
    
    
    //MARK:-  start session after
    @IBOutlet weak var capturecolcref: UICollectionView!
    @IBOutlet weak var startsessionViewref: UIView!
    @IBOutlet weak var startviewheightconstraintref: NSLayoutConstraint!
    @IBOutlet weak var startChattblref: UITableView!
    @IBOutlet weak var startChatHeaderviewref: UIView!
    @IBOutlet weak var imagesviewref: UIView!
    @IBOutlet weak var chatviewenableviewref: UIView!
    @IBOutlet weak var chatviewbackref: UIView!
    @IBOutlet weak var imageheightref: NSLayoutConstraint! //273
    @IBOutlet weak var chatbackviewheightref: NSLayoutConstraint! //144
    @IBOutlet weak var star_Chattfref: UITextField!
    @IBOutlet weak var star_Pepzidetailsviewref: UIView!
    
    @IBOutlet weak var ViewAllbtnref: UIButton!
    
    @IBOutlet weak var stopbtnref: UIButton!
    
    
    
    //MARK:-  Map relates varibales
    var distanceBetweenTwoCordinate = ""
    var geocodingDataTask: URLSessionDataTask?
    var SearchedLatCordinate = ""
    var SearchedLongCordinate = ""
    var SearchedPlaceName = ""
    var userCordinate:CLLocation?
    var searchedCordinate:CLLocation?
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    
    
    //MARK:-  An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    // The currently selected place.
    var selectedPlace: GMSPlace?
    
    
    //MARK:-  map searched place and distance
    var destination_addresses = ""
    var origin_addresses = ""
    var distanceBetweenPlace = ""
    var durationBetweenPlaces = ""
    var noLocation = false
    
    //MARK:-  All custom objects declaration
    var continueWithPapzies:NearByPapzies?
    var nearByPapzies:[NearByPapzies] = []
    var allMarker:[AllMarker] = []
    
    //MArk:- pepzi info for user
    var pepzi_profileurl = String()
    var issessionstarted = false
    
    //MARK:- chat Arr
    var chat_Arr :[chat_model] = []
    
    var collection_pos = Int()
    var index = Int()
    var bool = false
    
    
    //MARK:-  ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        mapView.clear()
        self.navigationController?.navigationBar.isHidden = true
        searchLocationNameView.isHidden = true
        btnHideLocationSearchView.isHidden = true
        let locationServerices = UserLocationManager.SharedManager
        locationSearchView.isHidden = true
        locationServerices.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateLcoationOnMap(_:)), name: NSNotification.Name(rawValue: kLocationDidChangeNotification), object: nil)
      
        
        
        
        let Image = UserDefaults.standard.string(forKey: UserConstant.profile_pic)!
        self.userImage.sd_setImage(with: URL(string: Image), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
        mapView.mapStyle1(withFilename: "map", andType: "json")
        placesClient = GMSPlacesClient.shared()
        mapView.isMyLocationEnabled = true
        mapView.padding = UIEdgeInsets(top: 0, left: 20, bottom: 300, right: 0)
        self.reloadToCurrentLocationOfuser()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        hiredPapziesVIew.addGestureRecognizer(tap)
        hiredPapziesVIew.isUserInteractionEnabled = true
        self.hiredPapziesVIew.clipsToBounds = true
        self.sendMessageView.clipsToBounds = true
        self.finalPaziesDeatilViewWithBackButton.isHidden = true
        self.finalPaziesDetailsView.isHidden = false
        self.bottomConstantOfHiredPapziesView.constant = -500
        self.tblChat.isHidden = true
        //start session
        let layout = AnimatedCollectionViewLayout()
        layout.scrollDirection = .horizontal
        layout.animator = PageAttributesAnimator()
        //layout.flipsHorizontallyInOppositeLayoutDirection = true
        layout.sectionFootersPinToVisibleBounds = true
        layout.sectionHeadersPinToVisibleBounds = true
        //= CGSize(width: 240, height: 340)
        layout.accessibilityContainerType = .list
        capturecolcref.collectionViewLayout = layout
        startsessionViewref.isHidden = true
        issessionstarted = true
        startChattblref.isHidden = true
        startChatHeaderviewref.isHidden = true
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(handleTap_afterstart(_:)))
        startsessionViewref.addGestureRecognizer(tap2)
        startsessionViewref.isUserInteractionEnabled = true
        self.startsessionViewref.clipsToBounds = true
        
        
        let sessionStatus = UserDefaults.standard.bool(forKey: UserConstant.Session_Status)
        if sessionStatus{
            let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
            let sessionId = UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? ""
            let role = UserDefaults.standard.string(forKey: UserConstant.user_type)
            SocketIOManager.sharedInstance.listnerForCheckSession(sessionId: sessionId,type:role!) { (response) in
                print(response)
                
                if response.isEmpty{
                    self.stopAnimating()
                }else{
                    self.stopAnimating()
                    self.slideView.isHidden = true
                    self.bottomConstantOfHiredPapziesView.constant = 20
                    self.finalPapziesImage.sd_setImage(with: URL(string: response["image"] as? String ?? ""), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
                    self.finalPapziesName2.text = "\(response["name"] as? String ?? "")"
                    self.finalPapziesName.text = "\(response["name"] as? String ?? "")"
                    self.waitingForPaziesRating.text = "0"
                    self.lblFinalLocationTo.text = "\(response["session_address"] as? String ?? "")"
                    self.lblFinalLocationFrom.text = "\(response["papzie_address"] as? String ?? "")"
                    let myCordinate = CLLocationCoordinate2D(latitude: Double(response["papzieLat"] as? String ?? "")!, longitude: Double(response["papzieLong"] as? String ?? "")!)
                    let distination = CLLocationCoordinate2D(latitude: Double(response["sessionLat"] as? String ?? "")!, longitude: Double(response["sessionLong"] as? String ?? "")!)
                    UserDefaults.standard.set(response["sessionId"] as? String ?? "", forKey: UserConstant.Session_id)
                    self.pepzi_profileurl = response["image"] as? String ?? ""
                    
                    UserDefaults.standard.set(true, forKey: UserConstant.Session_Status)
                    UserDefaults.standard.synchronize()
                    self.registerForGettingCurrentLocationOfPapzies(sessionId: sessionId)
                    self.draw(src: myCordinate, dst: distination, mode: "driving")
                    
                    let papziesLocation = GMSCameraPosition.camera(withLatitude: Double(response["papzieLat"] as? String ?? "")!, longitude: Double(response["papzieLong"] as? String ?? "")!, zoom: self.zoomLevel)
                    self.mapView.camera = papziesLocation
                    self.mapView.center = self.view.center
                    
                    SocketIOManager.sharedInstance.Joinchatroom(sessionId:response["sessionId"] as? String ?? ""){ (response) in
                        
                        if response.count > 0{
                            if let response = response[0] as? Array<Any>{
                                for dict in response{
                                   self.chat_Arr.append(chat_model(dict as! Dictionary))
                                }
                                
                                self.tblChat.reloadData()
                                self.startChattblref.reloadData()
                                self.scrollToBottom(self.tblChat)
                                self.scrollToBottom(self.startChattblref)
                            }else {
                                self.chat_Arr.append(chat_model(response[0] as! Dictionary))
                                self.tblChat.reloadData()
                                self.startChattblref.reloadData()
                                self.scrollToBottom(self.tblChat)
                                self.scrollToBottom(self.startChattblref)
                            }
                        }
                    
                    }
                    
                }
            }
            
            if socketConnected{
                SocketIOManager.sharedInstance.checkSession(sessionId: sessionId, type: role!)
            }
        }
        
      
    }
    
    
    //MARK:-  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        let userId = UserDefaults.standard.string(forKey: UserConstant.user_Id)
        SocketIOManager.sharedInstance.getAcceptandDeclineReponse(id: userId!) { (response) in
            print(response)
            let status = response["status"] as? String ?? ""
            if status == "0"{
                self.lblForWatingPapzies.text = "Oops! This Papzies has rejected your request"
                self.perform(#selector(self.hideViewAfterRejection), with: nil, afterDelay: 1)
            }else{
                let sessionId = response["sessionId"] as? String ?? ""
                UserDefaults.standard.set(sessionId, forKey: UserConstant.Session_id)
                UserDefaults.standard.set(true, forKey: UserConstant.Session_Status)
                UserDefaults.standard.synchronize()
                self.registerForGettingCurrentLocationOfPapzies(sessionId: sessionId)
                self.pepzi_profileurl = response["image"] as? String ?? ""
                self.bottomConstantForWatingPapziesView.constant = -200
                self.bottomConstantOfHiredPapziesView.constant = 20
                self.finalPapziesImage.sd_setImage(with: URL(string: response["image"] as? String ?? ""), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
                self.finalPapziesName2.text = "\(response["name"] as? String ?? "")"
                self.finalPapziesName.text = "\(response["name"] as? String ?? "")"
                self.waitingForPaziesRating.text = "0"
                SocketIOManager.sharedInstance.Joinchatroom(sessionId:sessionId){ (response) in
                    if response.count > 0{
                        if let response = response[0] as? Array<Any>{
                            for dict in response{
                                self.chat_Arr.append(chat_model(dict as! Dictionary))
                            }
                            self.tblChat.reloadData()
                            self.startChattblref.reloadData()
                            self.scrollToBottom(self.tblChat)
                            self.scrollToBottom(self.startChattblref)
                        }else {
                            self.chat_Arr.append(chat_model(response[0] as! Dictionary))
                            self.tblChat.reloadData()
                            self.startChattblref.reloadData()
                           self.scrollToBottom(self.tblChat)
                            self.scrollToBottom(self.startChattblref)
                        }
                    }
                }
            }
        }
    }
    
    func registerForGettingCurrentLocationOfPapzies(sessionId:String){
        SocketIOManager.sharedInstance.getCurrentLocationOFPazies(sessionId: sessionId) { (response) in
            print(response)
            if (response["status"] == nil){
                
            }else{
                
            }
            //     ["status": 0]
            self.slideView.isHidden = false
            UserDefaults.standard.set(sessionId, forKey: UserConstant.Session_id)
            UserDefaults.standard.set(true, forKey: UserConstant.Session_Status)
            self.bottomConstantOfHiredPapziesView.constant = -500
            self.mapView.clear()
            self.mapView.mapStyle1(withFilename: "map", andType: "json")
            self.mapView.isMyLocationEnabled = true
            self.reloadToCurrentLocationOfuser()
        }
        
    }
    
    func reloadToCurrentLocationOfuser(){
        
        let lat = UserDefaults.standard.double(forKey: "Lat")
        let Long = UserDefaults.standard.double(forKey: "Long")
        let camera = GMSCameraPosition.camera(withLatitude: (lat),
                                              longitude: (Long),
                                              zoom: zoomLevel)
        mapView.camera = camera
        self.bottomConstantForWatingPapziesView.constant = -300
        self.estimatedTotalPriceBottomConst.constant = -300
        self.collectionViewBottomConstant.constant = -250
        //        self.heightOfHiredPaziesVIew.constant = -300
        
    }
    //MARK:-  Hide View After papzies rejects
    @objc func hideViewAfterRejection(){
        
        self.bottomConstantForWatingPapziesView.constant = -200
        self.collectionViewBottomConstant.constant = 20
    }
    
    
    //MARK:-   delegate method from PepziesProfile Screen
    func hirePapzies(id: String,sessionTiming:String,distance:String,papziesLat:String,papziesLong:String,papzies:NearByPapzies?) {
        
        lableNearBySearchView.text = "Calculating Estimated Total Fare..."
        ImageNearBySearch.image = UIImage(named: "Dollar Loader")
        self.bottomConstantOfNearByPapziesView.constant = 20
        self.collectionViewBottomConstant.constant = -250
        var dist = distance
        if distance.contains("ft"){
            dist = "1"
        }else{
            if let range = distance.range(of: "mi") {
                dist.removeSubrange(range)
            }
            
        }
        hiringPaziesLat = papziesLat
        hiringPaziesLong = papziesLong
        
        self.getEstimatePrice(estimate_time: sessionTiming, distance: dist, papzies: papzies)
        
    }
    
    
    @objc func UpdateLcoationOnMap(_ notification:Notification) {
        
        print(notification.userInfo as! [String:Any])
        let Location = notification.userInfo as! [String:Any]
        let current = Location["location"] as! CLLocation
        let camera = GMSCameraPosition.camera(withLatitude: (current.coordinate.latitude),
                                              longitude: (current.coordinate.longitude),
                                              zoom: zoomLevel)
        mapView.camera = camera
        mapView.settings.myLocationButton = true
        mapView.isHidden = false
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }
    func listLikelyPlaces() {
        // Clean up from previous sessions.
        likelyPlaces.removeAll()
        
        placesClient.currentPlace(callback: { (placeLikelihoods, error) -> Void in
            if let error = error {
                print("Current Place error: \(error.localizedDescription)")
                return
            }
            
            // Get likely places and add to the list.
            if let likelihoodList = placeLikelihoods {
                for likelihood in likelihoodList.likelihoods {
                    let place = likelihood.place
                    self.likelyPlaces.append(place)
                }
            }
        })
    }
    
    //MARK:-   Show multiple market on googleMap
    
    func showMultipleMarker() {
        //        mapView.clear()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        for i in 0..<allMarker.count {
            let lat = allMarker[i].lat
            let long = allMarker[i].long
            let marker=GMSMarker()
            let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: 30, height: 30), image: UIImage(named: "cell-phone")!, borderColor: UIColor.darkGray, tag: i,name:"")
            marker.iconView=customMarker
            marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            marker.map = self.mapView
            marker.title = allMarker[i].name
            
        }
    }
}

//MARK:-  Uitextfield Delegate


//MARK:-   Location Update delegate method
extension UserDashboardVC : LocationUpdateProtocol{
    
    func locationDidUpdateToLocation(location: CLLocation) {
        
        self.currentLocation = location
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            DispatchQueue.main.async {
                self.mapView.isHidden = false
                self.mapView.camera = camera
            }
        } else {
            mapView.animate(to: camera)
        }
        //
        //        listLikelyPlaces()
        
    }
    
}
//MARK:-   Side menu delegate methods
extension UserDashboardVC:UserMenuDelegate {
    
    func UsercallMethod1(vcIndex: Int) {
        UsernavigationClass1(index:vcIndex)
    }
    func UserMenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(UserscreenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func UserscreenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            UsersharedMenuClass.menuObj.addMenuClass(view: self.view)
            UsersharedMenuClass.menuObj.menuView.Userdelegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func UsercallMethod(vcIndex:Int) {
        UsernavigationClass(index:vcIndex)
    }
    
    func UsertouchEvents(_ direction:String) {
        if direction == "left" {
            UsersharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            UsersharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

//MARK:-   Google map MapStyle Method
extension GMSMapView {
    func mapStyle1(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}
//MARK:-   Google map Auto place auto complete
extension UserDashboardVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        SearchedLatCordinate = "\(place.coordinate.latitude)"
        SearchedLongCordinate = "\(place.coordinate.longitude)"
        SearchedPlaceName = "\(place.name ?? "")"
        searchLocationNameView.isHidden = false
        txtSearchedName.text = place.name ?? ""
        let selectedPlace = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: zoomLevel)
        mapView.camera = selectedPlace
        
        let marker = GMSMarker()
        let markerImage = UIImage(named: "searchedPlace")!.withRenderingMode(.alwaysTemplate)
        let markerView = UIImageView(image: markerImage)
        marker.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        marker.iconView = markerView
        marker.title = SearchedPlaceName
        marker.map = mapView
        dismiss(animated: true) {
            
            if self.noLocation{
                self.txtSearchedName.text = ""
                self.searchLocationNameView.isHidden = true
                self.slideView.isHidden = false
                self.reloadToCurrentLocationOfuser()
                
            }else{
                self.estimateTimeViewConstant.constant = 20
                UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations: {
                    self.view.layoutIfNeeded()
                }) { (animationComplete) in
                    print("The animation is complete!")
                }
                
                
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true) {
            if self.SearchedPlaceName == ""{
                self.slideView.isHidden = false
            }
        }
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
//MARK:-   collectionView Datasource and Delegate
extension UserDashboardVC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

     /* -------------- display next friends action ----------------*/
    @IBAction func forwardbtnref(_ sender: Any) {
        // capturecolcref.scrollToNextItem()
        var int = 0
        int += 10
        collection_pos += 240 + int
      //  capturecolcref.moveToFrame(contentOffset: CGFloat(integerLiteral: collection_pos))
        capturecolcref.scrollToNextItem(exval: int, width: 8 * 240)
        //let indexPath = IndexPath(item: index + 1, section: 0)
              // self.capturecolcref.scrollToItem(at: indexPath , at: [.centeredVertically, .centeredHorizontally], animated: true)
    }
    
      /* -------------- display previous friends action ----------------*/
    @IBAction func backwordbtnref(_ sender: Any) {
       
        var int = 0
        int += 10
           capturecolcref.scrollToPreviousItem(exval : int ,width : 8 * 240)
       
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == capturecolcref{
            //MARK:- collection view
            //mark:-  taken pictures by pepzi showing in this collection view -- for user
            return CGSize(width: 240.0, height: 313.0)
        }else {
            return CGSize(width: 270.0, height: 170.0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == capturecolcref{
            //MARK:- collection view
            //mark:-  taken pictures by pepzi showing in this collection view -- for user
            return 8
        }else {
            return nearByPapzies.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       if collectionView == capturecolcref{
            //MARK:- collection view
            //mark:-  taken pictures by pepzi showing in this collection view -- for user
            let reuseIdentifier = "papzicaptureimgcell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! papzicaptureimgcell
             index = indexPath.row
            cell.papziimagref.image =  #imageLiteral(resourceName: "userType2")
            return cell
        }else  {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NearByUserViewCell", for: indexPath) as! NearByUserViewCell
            cell.papziesImage.sd_setImage(with: URL(string: self.nearByPapzies[indexPath.row].image), placeholderImage:UIImage(named: "Ellipse 319"), options: .highPriority, completed: nil)
            cell.lblName.text = "\(self.nearByPapzies[indexPath.row].fname) \(self.nearByPapzies[indexPath.row].lname)"
            cell.lblPaziesRate.text = self.nearByPapzies[indexPath.row].rating
            cell.lblDistance.text = self.nearByPapzies[indexPath.row].distance
            cell.lblPaziesAddress.text = self.nearByPapzies[indexPath.row].address
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == capturecolcref{
        } else {
            self.view.endEditing(true)
            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PapziesProfileVC") as! PapziesProfileVC
            Vc.papziesDetails = self.nearByPapzies[indexPath.row]
            Vc.SessionTiming = EstimateTime
            Vc.delegate = self
            self.navigationController?.pushViewController(Vc, animated: true)
        }
    }
    
}


//MARK:-   All Api call in this class
extension UserDashboardVC{
    
    func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D,mode: String){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=\(mode)&key=AIzaSyBr73R7SkyZkGSbLdeDEwl6n0jPC8SBfJo")!
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        self.estimatedTotalPriceBottomConst.constant = -300
                        let preRoutes = json["routes"] as! NSArray
                        let routes = preRoutes[0] as! NSDictionary
                        let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                        let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                        
                        //Distance
                        let legsDict = routes.value(forKey: "legs") as! NSArray
                        let durationDict = legsDict[0] as! NSDictionary
                        let durationvalueDict:NSDictionary = durationDict.value(forKey: "duration") as! NSDictionary
                        let Exactduration = durationvalueDict.object(forKey: "text") as! String
                        //Time
                        let DistancevalueDict:NSDictionary = durationDict.value(forKey: "distance") as! NSDictionary
                        let ExactDistance = DistancevalueDict.object(forKey: "text") as! String
                        DispatchQueue.main.async(execute: {
                            if ExactDistance != "" || Exactduration != "" {
                                //                                   self.timelblref.text = "Time Duration is :\(Exactduration)"
                                //                                   self.distancelblref.text = "Distance is :\(ExactDistance)"
                            }
                            //             if self.userusinggps == true {
                            //                                   self.navigationWithGPSline(polyStringd : polyString, src: CLLocationCoordinate2DMake(self.usergpslat,self.usergpslong),dst:CLLocationCoordinate2DMake(dst.latitude,dst.longitude))
                            //     }else {
                            print(polyString)
                            self.drawline(polyStringd : polyString, src: CLLocationCoordinate2DMake(src.latitude,src.longitude),dst:CLLocationCoordinate2DMake(dst.latitude,dst.longitude))
                            //   }
                        })
                    }
                    
                } catch {
                    print("parsing error")
                    
                }
            }
        })
        task.resume()
    }
    
    func drawline(polyStringd : String,src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D) {
        let cameraPositionCoordinates = CLLocationCoordinate2D(latitude: src.latitude, longitude: src.longitude)
        let cameraPosition = GMSCameraPosition.camera(withTarget: cameraPositionCoordinates, zoom: zoomLevel)
        mapView.isMyLocationEnabled = true
        mapView.camera = cameraPosition
        
        let marker3 = GMSMarker()
        marker3.icon = UIImage(named: "Drop")
        marker3.iconView?.backgroundColor = UIColor.green
        marker3.iconView?.tintColor = UIColor.green
        marker3.position = CLLocationCoordinate2DMake(dst.latitude,dst.longitude)
        marker3.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker3.map = mapView
        
        let marker1 = GMSMarker()
        marker1.icon = UIImage(named: "Drop")
        marker1.iconView?.backgroundColor = UIColor.green
        marker1.iconView?.tintColor = UIColor.green
        marker1.position = CLLocationCoordinate2DMake(src.latitude,src.longitude)
        marker1.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker1.map = mapView
        
        
        
        let path = GMSPath(fromEncodedPath: polyStringd)
        let rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 4.0
        rectangle.strokeColor = UIColor(named: "#0083CE")!
        rectangle.map = mapView
        
    }
    func getDistanceBetweenLatLong(myLat:String,myLong:String,la:String, lo:String, completionHandler: @escaping ((_ distance: String) -> Void)){
        
        
        let distanceApi = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(myLat),\(myLong)&destinations=\(la),\(lo)&key=AIzaSyBr73R7SkyZkGSbLdeDEwl6n0jPC8SBfJo"
        APIClass.apiClass.getRequest(controller: self, contentType: "application/json", method: "get", embedUrl: distanceApi, parameters: [:]) { (response, status) in
            
            print(status)
            print(response as! Dictionary<String,Any>)
            if status{
                if let data = response as? Dictionary <String,Any>{
                    if let desAddress = data["destination_addresses"] as? NSArray{
                        self.destination_addresses = desAddress[0] as? String ?? ""
                    }
                    if let originAdd = data["origin_addresses"] as? NSArray{
                        self.origin_addresses  = originAdd[0] as? String ?? ""
                    }
                    if let rows = data["rows"] as? NSArray {
                        if let fistRow = rows[0] as? Dictionary<String,Any>{
                            if let elements = fistRow["elements"] as? NSArray{
                                if let firstElements = elements[0] as? Dictionary<String,Any>{
                                    if let status = firstElements["status"] as? String{
                                        if status == "ZERO_RESULTS"{
                                            self.noLocation = true
                                            self.Alert(title: "Alert!", message: "Distance not available! please select other location")
                                            return
                                        }
                                    }
                                    if let distance = firstElements["distance"] as? Dictionary<String,Any>{
                                        self.distanceBetweenPlace = distance["text"] as? String ?? ""
                                        completionHandler(distance["text"] as? String ?? "")
                                    }
                                    if let duration = firstElements["duration"] as? Dictionary<String,Any>{
                                        self.durationBetweenPlaces = duration["text"] as? String ?? ""
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getEstimatePrice(estimate_time:String,distance:String,papzies:NearByPapzies? ){
        
        let param:[String:Any] = ["estimate_time":estimate_time,"distance":distance]
        let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
        
        APIClass.apiClass.PostRequest2(api: "private/calculate-fare", parameters: param, bearerToken: "Bearer \(BearerToken)") { (data, status) in
            
            if status{
                if let response = data as? Dictionary<String,Any>{
                    print(response)
                    if let status = response["status"] as? Int{
                        if status == 0{
                            let message = response["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                            return
                        }else{
                            if let payload = response["payload"] as? Dictionary<String,Any>{
                                self.bottomConstantOfNearByPapziesView.constant = -200
                                self.estimatedTotalPriceBottomConst.constant = 20
                                self.lblDistanceFare.text = "$\(payload["distance_fare"] as? Double ?? 0.0)"
                                self.lblPapziesFare.text = "$\(payload["papzie_fare"] as? Double ?? 0.0)/ min"
                                self.lblTotalSessionTime.text = "\(payload["session_time"] as? Double ?? 0.0) min"
                                self.lblTotalEstimatedFare.text = "$\(payload["estimated_fare"] as? Double ?? 0.0)"
                                self.continueWithPapzies = papzies
                                
                            }
                        }
                    }
                }
            }else{
                self.Alert(title: "Alert!", message: "Something went to wrong")
            }
        }
    }
    
    func searchNearBypapzies(){
        
        let param:[String:Any] = ["latitude":self.SearchedLatCordinate,"longitude":self.SearchedLongCordinate]
        let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
        
        APIClass.apiClass.PostRequest2(api: "private/nearby-papzies", parameters: param, bearerToken: "Bearer \(BearerToken)") { (data, status) in
            
            if status{
                if let response = data as? Dictionary<String,Any>{
                    print(response)
                    if let status = response["status"] as? Int{
                        if status == 0{
                            let message = response["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                            return
                        }else{
                            if let payload = response["payload"] as? NSArray{
                                if payload.count == 0{
                                    self.estimateTimeViewConstant.constant = 20
                                    self.lableNearBySearchView.text = "Searching for the Nearby papzies..."
                                    self.ImageNearBySearch.image = UIImage(named: "Dummy Profile loader")
                                    self.bottomConstantOfNearByPapziesView.constant = -200
                                    self.collectionViewBottomConstant.constant = -250
                                    
                                    self.Alert(title: "Alert!", message: "No Papzies around searched location")
                                    return
                                }else{
                                    for dict in payload{
                                        let dictData = dict as! Dictionary<String,Any>
                                        let lat = dictData["latitude"] as? String ?? ""
                                        let long = dictData["longitude"] as? String ?? ""
                                        self.allMarker.append(AllMarker(lat: Double(lat)!, long: Double(long)!,name:dictData["fname"] as? String ?? "" ))
                                        let lat1 = "\(UserDefaults.standard.double(forKey: "Lat"))"
                                        let Long = "\(UserDefaults.standard.double(forKey: "Long"))"
                                        
                                        self.getDistanceBetweenLatLong(myLat:lat1,myLong:Long, la: lat, lo: long) { (distance) in
                                            self.nearByPapzies.append(NearByPapzies(value: dictData ,dist:distance))
                                            DispatchQueue.main.async {
                                                
                                                self.bottomConstantOfNearByPapziesView.constant = -200
                                                self.collectionViewBottomConstant.constant = 20
                                                self.collectionView.reloadData()
                                            }
                                        }
                                    }
                                    self.showMultipleMarker()
                                }
                            }
                        }
                    }
                }
            }else{
                self.estimateTimeViewConstant.constant = 20
                self.lableNearBySearchView.text = "Searching for the Nearby papzies..."
                self.ImageNearBySearch.image = UIImage(named: "Dummy Profile loader")
                self.bottomConstantOfNearByPapziesView.constant = -200
                self.collectionViewBottomConstant.constant = -250
                
                self.Alert(title: "Alert!", message: "Something went to wrong")
            }
        }
    }
    
}
extension UserDashboardVC:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtSendMessage{
            self.heightOfHiredPaziesVIew.constant = 432
            heightOfCardView.constant = 50
            self.finalPaziesDeatilViewWithBackButton.isHidden = false
            self.finalPaziesDetailsView.isHidden = true
            cardDetailView.isHidden = true
            LocationView.isHidden = true
            startButtonView.isHidden = true
            self.tblChat.isHidden = false
        }else if textField == self.star_Chattfref {
            self.startviewheightconstraintref.constant = 550
            self.star_Pepzidetailsviewref.isHidden = true
            self.star_Pepzidetailsviewref.clipsToBounds = true
            self.chatbackviewheightref.constant = 60
           // self.imageheightref.constant = 0
            self.imagesviewref.isHidden = true
            self.startChattblref.reloadData()
            self.chatviewbackref.clipsToBounds = true
            self.imagesviewref.clipsToBounds = true
            self.startChattblref.isHidden = false
            self.startChatHeaderviewref.isHidden = false
            self.ViewAllbtnref.isHidden = true
            self.stopbtnref.isHidden = true
            self.chatviewenableviewref.backgroundColor = .white
            self.chatviewenableviewref.borderWidth = 1
            self.scrollToBottom(self.tblChat)
            self.scrollToBottom(self.startChattblref)
        }
    }
}


//MARK:-   All Button Action methods
extension UserDashboardVC{
    
    /*---------------send message button----------------*/
    @IBAction func cmdSendMessage(_ sender: Any) {
        if self.txtSendMessage.text ?? "" != "" {
            SocketIOManager.sharedInstance.sendmessage(roomId:UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? "",type: "1",message : self.txtSendMessage.text ?? "")
            self.txtSendMessage.text = ""
        }else if self.star_Chattfref.text ?? "" != "" {
            SocketIOManager.sharedInstance.sendmessage(roomId:UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? "",type: "1",message : self.star_Chattfref.text ?? "")
            self.star_Chattfref.text = ""
        }
    }
    /*---------------open chat seen button----------------*/
    @IBAction func cmdFinalPapziesOpenChat(_ sender: Any) {
        self.heightOfHiredPaziesVIew.constant = 432
        heightOfCardView.constant = 50
        self.finalPaziesDeatilViewWithBackButton.isHidden = false
        self.finalPaziesDetailsView.isHidden = true
        cardDetailView.isHidden = true
        LocationView.isHidden = true
        startButtonView.isHidden = true
    }
    /*--------------back from chat to hire pepzi seen or startview-------*/
    @IBAction func cmdFinalPapziesback(_ sender: Any) {
        self.finalPaziesDeatilViewWithBackButton.isHidden = true
        self.finalPaziesDetailsView.isHidden = false
        cardDetailView.isHidden = false
        LocationView.isHidden = false
        startButtonView.isHidden = false
        self.tblChat.isHidden = true
        self.txtSendMessage.resignFirstResponder()
    }
   /*--------------call to pepzi---------  */
    @IBAction func cmdFinalPapziesCall(_ sender: Any) {
       // self.heightOfHiredPaziesVIew.constant = 432
    }
     /*--------------Start session-------*/
    @IBAction func cmdFinalPapziesStartSession(_ sender: Any) {
          self.heightOfHiredPaziesVIew.constant = 0
          self.startsessionViewref.isHidden = false
    }
    
    //MARK:-  after start session function which is triggered when handleTap is called
    @objc func handleTap_afterstart(_ sender: UITapGestureRecognizer) {
        self.startChattblref.isHidden = true
        self.startChatHeaderviewref.isHidden = true
        self.star_Chattfref.resignFirstResponder()
        self.star_Pepzidetailsviewref.isHidden = false
        self.ViewAllbtnref.isHidden = false
        self.stopbtnref.isHidden = false
        self.chatviewenableviewref.backgroundColor = UIColor(named: "#303030")
        self.chatviewenableviewref.borderWidth = 0
        self.chatbackviewheightref.constant = 144
        if self.startviewheightconstraintref.constant == 133{
            self.startviewheightconstraintref.constant = 550
        }else{
            self.startviewheightconstraintref.constant = 133
              self.chatviewbackref.clipsToBounds = true
              self.imagesviewref.clipsToBounds = true
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (status) in
            print(status)
            if status {
                        if self.startviewheightconstraintref.constant == 133{
                    self.imagesviewref.isHidden = true
                    self.chatviewbackref.isHidden = true
                    self.chatviewbackref.clipsToBounds = true
                    self.imagesviewref.clipsToBounds = true
                }else {
                    self.imagesviewref.isHidden = false
                    self.chatviewbackref.isHidden = false
                    self.chatviewbackref.clipsToBounds = false
                    self.imagesviewref.clipsToBounds = false
                }
            }else {
                
            }

        }
     
    }
   
     //MARK:-  function which is triggered when handleTap is called
     @objc func handleTap(_ sender: UITapGestureRecognizer) {
         self.tblChat.isHidden = true
        self.txtSendMessage.resignFirstResponder()
         print("Hello World")
         if self.heightOfHiredPaziesVIew.constant == 100{
             self.heightOfHiredPaziesVIew.constant = 432
             heightOfCardView.constant = 50
             //  self.txtSendMessage.resignFirstResponder()
         }else{
             heightOfCardView.constant = 0
             self.heightOfHiredPaziesVIew.constant = 100
             self.finalPaziesDeatilViewWithBackButton.isHidden = true
             self.finalPaziesDetailsView.isHidden = false
             cardDetailView.isHidden = false
             LocationView.isHidden = false
             startButtonView.isHidden = false
         }
         UIView.animate(withDuration:0.2, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
             self.view.layoutIfNeeded()
         }, completion: nil)
     }
    
    
    
    
    
    @IBAction func cmdCancelForWatingPapzies(_ sender: Any) {
        
        
        
        let alert = UIAlertController(title: "Cancel", message: "Are your sure want to cancel session?",  preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.destructive, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "YES",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.bottomConstantForWatingPapziesView.constant = -200
                                        self.slideView.isHidden = false
                                        self.searchLocationNameView.isHidden = true
                                        self.txtSearchedName.text = ""
                                        self.txtEnterTime.text = ""
                                        self.mapView.clear()
                                        self.mapView.mapStyle1(withFilename: "map", andType: "json")
                                        self.mapView.isMyLocationEnabled = true
                                        self.reloadToCurrentLocationOfuser()
                                        let sessionId = UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? ""
                                        SocketIOManager.sharedInstance.cancelAcceptedSession(sessionId: sessionId)
                                        UserDefaults.standard.set("", forKey: UserConstant.Session_id)
                                        UserDefaults.standard.set(false, forKey: UserConstant.Session_Status)
                                        
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func cmdEstimateTotalFareBack(_ sender: Any) {
        self.collectionViewBottomConstant.constant = 20
        self.estimatedTotalPriceBottomConst.constant = -300
    }
    
    
    // Proceed Button
    @IBAction func cmdContinueFromTotalEstimateFare(_ sender: Any) {
        
        let lat = UserDefaults.standard.double(forKey: "Lat")
        let Long = UserDefaults.standard.double(forKey: "Long")
        self.estimatedTotalPriceBottomConst.constant = -300
        self.bottomConstantForWatingPapziesView.constant = 20
        self.lblForWatingPapzies.text = "Waiting for your Papzies to respond..."
        self.waitingForPapziesImage.sd_setImage(with: URL(string: self.continueWithPapzies!.image), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
        self.waitingForPapziesName.text = "\(continueWithPapzies!.fname) \(continueWithPapzies!.lname)"
        self.waitingForPaziesRating.text = continueWithPapzies!.rating
        let myCordinate = CLLocationCoordinate2D(latitude: Double(self.SearchedLatCordinate)!, longitude: Double(self.SearchedLongCordinate)!)
        let distination = CLLocationCoordinate2D(latitude: Double(hiringPaziesLat)!, longitude: Double(hiringPaziesLong)!)
        self.draw(src: myCordinate, dst: distination, mode: "driving")
        let userid = UserDefaults.standard.string(forKey: UserConstant.user_Id)
        self.getDistanceBetweenLatLong(myLat: self.SearchedLatCordinate, myLong:  self.SearchedLongCordinate, la: self.continueWithPapzies!.latitude, lo: self.continueWithPapzies!.longitude) { (distance) in
            var dist = distance
            if dist.contains("ft"){
                dist = "1"
            }else{
                if let range = distance.range(of: "mi") {
                    dist.removeSubrange(range)
                }
                
            }
            SocketIOManager.sharedInstance.HirePapzies(id: userid!, receiver_id: self.continueWithPapzies!.id, estimated_time: self.EstimateTime, latitude: self.SearchedLatCordinate, longitude: self.SearchedLongCordinate, distance:dist )
            
        }
        
    }
    
    
    @IBAction func cmdMenu(_ sender: Any) {
        UsersharedMenuClass.menuObj.addMenuClass(view: self.view)
        UsersharedMenuClass.menuObj.menuView.Userdelegate = self
        
    }
    @IBAction func cmdUserImage(_ sender: Any) {
        
    }
    
    @IBAction func cmdSearchLocation(_ sender: Any) {
        btnHideLocationSearchView.isHidden = true
        locationSearchView.isHidden = true
        slideView.isHidden = true
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        placePickerController.modalPresentationStyle = .overFullScreen
        present(placePickerController, animated: true, completion: nil)
        
    }
    @IBAction func cmdSlideToHire(_ sender: Any) {
        
        btnHideLocationSearchView.isHidden = false
        locationSearchView.isHidden = false
        btnHideLocationSearchView.isHidden = false
        slideView.isHidden = true
        self.locationSearchBottomConstant.constant = 0
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            self.view.endEditing(false)
            self.txtLocation.becomeFirstResponder()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    
    @IBAction func cmdHideLocationSearchView(_ sender: Any) {
        btnHideLocationSearchView.isHidden = true
        locationSearchView.isHidden = true
        btnHideLocationSearchView.isHidden = true
        slideView.isHidden = false
        self.locationSearchBottomConstant.constant = -130
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
        
    }
    
    @IBAction func cmdAbortEstimateView(_ sender: Any) {
        self.estimateTimeViewConstant.constant = -200
        self.txtEnterTime.text = ""
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
            self.txtSearchedName.text = ""
            self.searchLocationNameView.isHidden = true
            self.slideView.isHidden = false
            self.reloadToCurrentLocationOfuser()
        }
    }
    
    @IBAction func cmdContinue(_ sender: Any) {
        if txtEnterTime.text == ""{
            self.Alert(title: "Alert!", message: "Please enter Estimated Session Timing")
            return
        }else{
            self.txtEnterTime.resignFirstResponder()
            EstimateTime = txtEnterTime.text!
            self.estimateTimeViewConstant.constant = -200
            lableNearBySearchView.text = "Searching for the Nearby papzies..."
            ImageNearBySearch.image = UIImage(named: "Dummy Profile loader")
            self.bottomConstantOfNearByPapziesView.constant = 20
            self.collectionViewBottomConstant.constant = 20
            self.searchNearBypapzies()
            
        }
    }
    
    
}



//MARK:- chat module


extension UserDashboardVC : UITableViewDelegate,UITableViewDataSource{
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.chat_Arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = self.chat_Arr[indexPath.row].type
        if "1" == type{
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "sendercell") as! sendercell
            cell1.senderlblref.text = self.chat_Arr[indexPath.row].msg
            let Image = UserDefaults.standard.string(forKey: UserConstant.profile_pic)!
            cell1.senderimgref.sd_setImage(with: URL(string: Image), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
            return cell1
        }else {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "ReceverCell") as! ReceverCell
            cell2.receverlblref.text  = self.chat_Arr[indexPath.row].msg
            cell2.receverimgref.sd_setImage(with: URL(string:  self.pepzi_profileurl), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
            return cell2
        }
    }

    
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
  }

  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      return 1000.0
 }
    
    func scrollToBottom(_ tbl : UITableView){
        let indexPath = NSIndexPath(item: self.chat_Arr.count-1, section: 0)
        tbl.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: false)
    }

}





extension UserDashboardVC {
    
}

extension UICollectionView {
    func scrollToNextItem(exval : Int ,width : Int) {
         let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        if contentOffset < CGFloat(integerLiteral: width)   {
        self.moveToFrame(contentOffset: contentOffset + CGFloat(integerLiteral: exval))
        }
    }
    func scrollToPreviousItem(exval : Int ,width : Int)  {
         let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
        let width_screen  = CGFloat(integerLiteral: width) + contentOffset
        if width_screen > 0 && contentOffset != CGFloat(integerLiteral: -240) && self.contentOffset.x > 0 && self.contentOffset.x > 240 {
            if self.contentOffset.x > 240 {
                 self.moveToFrame(contentOffset: contentOffset)
            }else {
                 self.moveToFrame(contentOffset: contentOffset - CGFloat(integerLiteral: exval))
            }
        }
     }
    func moveToFrame(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
    }
}

  

