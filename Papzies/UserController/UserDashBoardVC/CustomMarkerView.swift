//
//  CustomMarkerView.swift
//  googlMapTutuorial2
//
//  Created by Muskan on 12/17/17.
//  Copyright © 2017 akhil. All rights reserved.
//

import Foundation
import UIKit

class CustomMarkerView: UIView {
    var img: UIImage!
    var name:String!
    var borderColor1: UIColor!
    
    init(frame: CGRect, image: UIImage, borderColor: UIColor, tag: Int,name:String!) {
        super.init(frame: frame)
        self.img=image
        self.borderColor1=borderColor
        self.tag = tag
        setupViews()
    }
    
    func setupViews() {
        let imgView = UIImageView(image: img)
        imgView.frame=CGRect(x: 0, y: 0, width: 7.13, height: 12.59)
        imgView.layer.borderColor=borderColor1?.cgColor
        imgView.clipsToBounds=true
        self.addSubview(imgView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MyCustomMarkerView: UIView {
    var img: UIImage!
    var border: UIColor!
    
    init(frame: CGRect, image: UIImage, borderColor: UIColor, tag: Int) {
        super.init(frame: frame)
        self.img=image
        self.border=borderColor
        self.tag = tag
        setupViews()
    }
    
    func setupViews() {
        let imgView = UIImageView(image: img)
        imgView.frame=CGRect(x: 0, y: 0, width: 24, height: 24)
        imgView.layer.cornerRadius = 12
        imgView.layer.borderColor=border?.cgColor
        imgView.layer.borderWidth=1
        imgView.clipsToBounds=true
        self.addSubview(imgView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}









