//
//  NearByUserViewCell.swift
//  Papzies
//
//  Created by apple on 17/01/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class NearByUserViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPaziesAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblPaziesRate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var papziesImage: UIImageView!
}


