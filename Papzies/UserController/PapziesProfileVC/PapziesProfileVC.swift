//
//  PapziesProfileVC.swift
//  Papzies
//
//  Created by apple on 22/01/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

protocol HirePapzies {
    func hirePapzies(id:String,sessionTiming:String,distance:String,papziesLat:String,papziesLong:String,papzies:NearByPapzies?)
}

class PapziesProfileVC: UIViewController {

    var SessionTiming = ""
    var delegate:HirePapzies?
    @IBOutlet weak var papziesDiscription: UILabel!
    @IBOutlet weak var papziesRating: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var papziesImage: UIImageView!
    @IBOutlet weak var collcetionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    var papziesDetails:NearByPapzies?
    @IBOutlet weak var btnHire: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDetails()
    }
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setDetails(){
        lblName.text = "\(papziesDetails?.fname ?? "") \(papziesDetails?.lname ?? "")"
        papziesRating.text = "\(papziesDetails?.rating ?? "")"
        self.papziesImage.sd_setImage(with: URL(string: (papziesDetails?.image)!), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
        self.papziesDiscription.text = papziesDetails?.bio
    }
    
    @IBAction func cmdHire(_ sender: Any) {
        
        delegate?.hirePapzies(id:papziesDetails?.id ?? "", sessionTiming: SessionTiming,distance:papziesDetails?.distance ?? "", papziesLat: papziesDetails?.latitude ?? "", papziesLong: papziesDetails?.longitude ?? "", papzies: self.papziesDetails!)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension PapziesProfileVC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 85.0, height: 85.0)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (papziesDetails?.images.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaziesPhotosViewCell", for: indexPath) as! PaziesPhotosViewCell
        cell.photos.sd_setImage(with: URL(string: (papziesDetails?.images[indexPath.row])!), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
        return cell
    }
    
    
    
}


