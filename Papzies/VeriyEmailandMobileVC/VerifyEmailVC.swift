//
//  VerifyEmailVC.swift
//  Papzies
//
//  Created by apple on 10/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class VerifyEmailVC: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var btnHideTick: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtFourth: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    var emailAddress = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnHideTick.isHidden = true
        self.tickImage.isHidden = true
        txtFirst.delegate = self
        txtSecond.delegate = self
        txtThird.delegate = self
        txtFourth.delegate = self
        self.lblEmail.text = emailAddress
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cmdBack(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PapziesSignupVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func cmdSubmit(_ sender: Any) {
        self.view.endEditing(true)
        textFieldValidation()
        
    }
    @IBAction func cmdResend(_ sender: Any) {
        self.resendOTP()
    }
    @IBAction func cmdHideTick(_ sender: Any) {
    }
    
    func textFieldValidation(){
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            if txtField.isEmpty{
                self.Alert(title: "Alert!", message: "Please enter correct verification code")
                return
            }
        }
        self.verifyEmailOTP()
    }
    
    func resendOTP(){
        let OTP = "\(txtFirst.text!)\(txtSecond.text!)\(txtThird.text!)\(txtFourth.text!)"
        let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
        let param:[String:Any] = ["token":OTP]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "private/resend-email-code", parameters: param, bearerToken: "Bearer \(BearerToken)"){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            if let payload = data["payload"] as? Dictionary<String,Any>{
                                self.Alert(title: "Alert!", message: "OTP Send to registered email address")
                            }
                        }
                    }
                }
            }
        }
    }

    

    func verifyEmailOTP(){
        let OTP = "\(txtFirst.text!)\(txtSecond.text!)\(txtThird.text!)\(txtFourth.text!)"
        let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
        let param:[String:Any] = ["token":OTP]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "private/verify-email", parameters: param, bearerToken: "Bearer \(BearerToken)"){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            if let payload = data["payload"] as? Dictionary<String,Any>{
                                self.animate()
                            }
                        }
                    }
                }
            }
        }
    }

    
    func animate(){
        
        btnHideTick.isHidden = false
        self.tickImage.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            UIView.animate(withDuration: 0.2,
                           animations: {
                            
            },
                           completion: { finished in
                            self.btnHideTick.isHidden = true
                            self.tickImage.isHidden = true
                            self.Navigate()
            })
        }
    }
    
    func Navigate(){
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "PapziesDescriptionVC") as! PapziesDescriptionVC
        self.navigationController?.pushViewController(Vc, animated: true)

    }
    


}

extension VerifyEmailVC:UITextFieldDelegate{

    @IBAction func textEditDidBegin(_ sender: UITextField) {
        print("textEditDidBegin has been pressed")
        
        if !(sender.text?.isEmpty)!{
            sender.selectAll(self)
            //buttonUnSelected()
        }else{
            print("Empty")
            sender.text = " "
            
        }
        
    }
    @IBAction func textEditChanged(_ sender: UITextField) {
        print("textEditChanged has been pressed")
        let count = sender.text?.count
        //
        if count == 1{
            
            switch sender {
            case txtFirst:
                txtSecond.becomeFirstResponder()
            case txtSecond:
                txtThird.becomeFirstResponder()
            case txtThird:
                txtFourth.becomeFirstResponder()
            case txtFourth:
                txtFourth.resignFirstResponder()
            default:
                print("default")
            }
        }
        
    }

        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

            textField.text = ""
            if textField.text == "" {
                print("Backspace has been pressed")
            }
            
            if string == ""
            {
                print("Backspace was pressed")
                switch textField {
                case txtSecond:
                    txtFirst.becomeFirstResponder()
                case txtThird:
                    txtSecond.becomeFirstResponder()
                case txtFourth:
                    txtThird.becomeFirstResponder()
                default:
                    print("default")
                }
                textField.text = ""
                return false
            }
            
            return true
    }
}

