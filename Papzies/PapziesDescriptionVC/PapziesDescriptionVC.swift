//
//  PapziesDescriptionVC.swift
//  Papzies
//
//  Created by apple on 10/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PapziesDescriptionVC: UIViewController,NVActivityIndicatorViewable {

    
    @IBOutlet weak var txtDiscription: UITextView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtDiscription.delegate = self
    }
    
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cmdContinue(_ sender: Any) {
        self.view.endEditing(true)
        self.textFieldValidation()
    }
    func textFieldValidation(){
        if txtDiscription.text == "Hiiiii....." || txtDiscription.text == ""{
            self.Alert(title: "Alert!", message: "Please enter a good discription")
        }else{
            self.updateUserBio()
        }
    }

    
    func updateUserBio(){
        let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
        let param:[String:Any] = ["bio":txtDiscription.text!]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "private/update-bio", parameters: param, bearerToken: "Bearer \(BearerToken)"){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPictureVC") as! SelectPictureVC
                            UserDefaults.standard.set(self.txtDiscription.text!, forKey: "Bio")
                            UserDefaults.standard.synchronize()
                            self.navigationController?.pushViewController(Vc, animated: true)
                        }
                        }
                }else{
                    self.Alert(title: "Alert!", message: "Somthing went to wrong")
                }
                }
            }
        }
    }

extension PapziesDescriptionVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Hiiiii.....")
        {
            textView.text = ""
            textView.textColor = .black
        }
        textView.becomeFirstResponder()
    }

    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = "Hiiiii....."
            textView.textColor = .lightGray
        }
        textView.resignFirstResponder()
    }

}
