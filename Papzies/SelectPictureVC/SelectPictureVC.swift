//
//  SelectPictureVC.swift
//  Papzies
//
//  Created by apple on 10/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import NVActivityIndicatorView

class SelectPictureVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,NVActivityIndicatorViewable {

    var picker:UIImagePickerController = UIImagePickerController()
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    var userImagePicked = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        userImage.isUserInteractionEnabled = true
        userImage.addGestureRecognizer(tapGestureRecognizer)

    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.popImageChooser()

    }

    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cmdCapture(_ sender: Any) {
        self.updateUserImage()
        
//        if UIImagePickerController.isSourceTypeAvailable(.camera){
//            self.picker.delegate = self
//            self.picker.sourceType = .camera;
//            self.picker.cameraDevice = .front
//            self.picker.allowsEditing = true
//            self.picker.mediaTypes = [kUTTypeImage] as [String]
//            self.present(self.picker, animated: true, completion: nil)
//        }else{
//            self.Alert(title: "Alert!", message: "Camera Unavailable")
//        }

    }
    
    func popImageChooser(){
         self.picker.delegate = self
        let alert = UIAlertController(title: "Papzies", message: "Choose media type", preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Photos", style: .default) { (handler) in
        self.picker.isEditing = true
        self.picker.sourceType = .photoLibrary
        self.picker.mediaTypes = [kUTTypeImage] as [String]
        self.present(self.picker, animated: true, completion: nil)
        }
        
        let gallery = UIAlertAction(title: "Camera", style: .default) { (handler) in
            self.goToCamera()
        }
        
        let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (handler) in
        }
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(Cancel)
        self.present(alert, animated: true)

    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let pickedImage = info[.originalImage] as? UIImage
        self.userImage.image = pickedImage
        userImagePicked = true
        dismiss(animated: true, completion: nil)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        } else {
            self.Alert(title: "Warning", message: "You don't have camera")
        }
    }

    func goToCamera()
    {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch (status)
        {
        case .authorized:
            self.openCamera()

        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
                if (granted)
                {
                    self.openCamera()
                }
                else
                {
                    self.camDenied()
                }
            }

        case .denied:
            self.camDenied()

        case .restricted:
            let alert = UIAlertController(title: "Restricted",
                                          message: "You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.",
                                          preferredStyle: .alert)

            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
           // viewController.present(alert, animated: true, completion: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
        func camDenied()
        {
            DispatchQueue.main.async
                {
                    var alertText = "It looks like your privacy settings are preventing us from accessing your camera to capture photo. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Camera on.\n\n5. Open this app and try again."

                    var alertButton = "OK"
                    var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)

                    if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
                    {
                        alertText = "It looks like your privacy settings are preventing us from accessing your camera to capture photo. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again."

                        alertButton = "Go"

                        goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:]
    , completionHandler: nil)
                        })
                    }

                    let alert = UIAlertController(title: "Error", message: alertText, preferredStyle: .alert)
                    alert.addAction(goAction)
                  //  viewController.present(alert, animated: true, completion: nil)
                    self.present(alert, animated: true, completion: nil)
            }
        }




    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        userImagePicked = false
        self.dismiss(animated: true, completion: nil)
    }

        func updateUserImage(){
            if userImagePicked{
                let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!
                let size = CGSize(width: 30, height: 30)
                startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
                var imageData:Data!
                imageData = userImage.image!.pngData()!
                //let param:[String:Any] = ["file":""]
                
                APIClass.apiClass.requestPostWithMultiFormData("http://3.136.221.59:3000/private/upload-profile-image", params: nil, imagesfile:userImage!.image!, keyNameForImage: "file",bearerToken:BearerToken, completionHandler: { (data, status) in
                        if status{
                            self.stopAnimating()
                        if let data = data as? Dictionary<String,Any>{
                                            print(data)
                                            if let status = data["status"] as? Int{
                                                if status == 0{
                                                    let message = data["message"] as! String
                                                    self.Alert(title: "Alert!", message: message)
                                                }else{
                                                    if let payload = data["payload"] as? String{
                                                        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "TermConditionsVC") as! TermConditionsVC
                                                        UserDefaults.standard.set(payload, forKey: UserConstant.profile_pic)
                                                        self.navigationController?.pushViewController(Vc, animated: true)

                                                    }
                                               }
                                            }
                                        }
                                    }else{
                                        self.stopAnimating()
                                        self.Alert(title: "Alert!", message: "Something went wrong")
                                    }

                }) { (error) in
                    print(error)
                }
            
//            APIClass.apiClass.requestWith(endUrl: "private/upload-profile-image", fileName: ProcessInfo.processInfo.globallyUniqueString, imageData: imageData, fileType: "jpeg/png", parameters:nil , keyvalue: "file",bearerToken:BearerToken) { (data, status) in
//
//                if status{
//                    self.stopAnimating()
//                    if let data = data as? Dictionary<String,Any>{
//                        print(data)
//                        if let status = data["status"] as? Int{
//                            if status == 0{
//                                let message = data["message"] as! String
//                                self.Alert(title: "Alert!", message: message)
//                            }else{
//                                let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPictureVC") as! SelectPictureVC
//                                self.navigationController?.pushViewController(Vc, animated: true)
//                            }
//                        }
//                    }
//                }else{
//                    self.stopAnimating()
//                    self.Alert(title: "Alert!", message: "Something went wrong")
//                }
//            }
        }else{
            self.Alert(title: "Alert!", message: "Please choose the profile picture.")
        }
    }
}
