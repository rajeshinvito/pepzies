//
//  ResetPasswordVC.swift
//  Papzies
//
//  Created by apple on 17/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ResetPasswordVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    var mobileNumber = ""
    var token = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func cmdBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldValidation(){
        let allTextField = getTextfield(view: self.view)
        for txtField in allTextField
        {
            if txtField.isEmpty{
                self.Alert(title: "Alert!", message: "Please enter \(txtField.placeholder!)")
                return
            }
        }
        if txtNewPassword.text != txtConfirmPassword.text{
            self.Alert(title: "Alert!", message: "Please new and confirm password do not match")
            return
        }
        self.resetPassword()
    }
    
    
    @IBAction func cmdChange(_ sender: Any) {
        self.textFieldValidation()
    }
    
    func resetPassword(){
        
        let param:[String:Any] = ["token":self.token,"mobile":mobileNumber,"password":txtNewPassword.text!]
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
        APIClass.apiClass.PostRequest2(api: "reset-password", parameters: param, bearerToken: ""){(response, status) in
            if status{
                self.stopAnimating()
                if let data = response as? Dictionary<String,Any>{
                    print(data)
                    if let status = data["status"] as? Int{
                        if status == 0{
                            let message = data["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                        }else{
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: LoginVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
