//
//  PapziesHomeVC.swift
//  Papzies
//
//  Created by apple on 13/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import MapKit
import LabelSwitch
import GoogleMaps
import GooglePlaces
import NVActivityIndicatorView

class PapziesHomeVC: UIViewController,NewRequestAccepted,NVActivityIndicatorViewable {

    var detailsOfAcceptedPazies:[String:Any]?
    
    //MARK:-   ShutterView view
    
    @IBOutlet weak var shutterViewBottomConstant: NSLayoutConstraint!
    @IBOutlet weak var btnSutter: UIButton!
    
    
    //MARK:-   hired user session view
    @IBOutlet weak var hiredUserAllDetailsView: UIView!
    @IBOutlet weak var hiredUserVIew: UIView!
    @IBOutlet weak var heightOfHiredUserVIew: NSLayoutConstraint!
    @IBOutlet weak var bottomConstantOfHiredUserView: NSLayoutConstraint!
    @IBOutlet weak var FirstsendMessageView: UIView!
    @IBOutlet weak var heightOfUserDetailsView: NSLayoutConstraint!
    @IBOutlet weak var UserDeatilsViewWithBackButton: UIView!
    @IBOutlet weak var heightOfUserDetailsViewWithBackButton: NSLayoutConstraint!
    @IBOutlet weak var heightOfSendMessageView: NSLayoutConstraint!
    @IBOutlet weak var sendMessageView: UIView!
    
    @IBOutlet weak var finalHiredUserName2: UILabel!
    @IBOutlet weak var finalHiredUserImage: UIImageView!
    @IBOutlet weak var finalHiredUserName: UILabel!
    @IBOutlet weak var finalHiredUserRating: UILabel!
    @IBOutlet weak var btnFinalUserOpenChat: UIButton!
    @IBOutlet weak var btnFinalUserCall: UIButton!
    @IBOutlet weak var btnFinalUserCancelView: UIButton!
    
    @IBOutlet weak var btnFinalUserBack: UIButton!
    @IBOutlet weak var finalUserLocation: UILabel!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtSendMessage: UIView!
    @IBOutlet weak var btnSendMessage: UIButton!
    
    @IBOutlet weak var tfsendmessageref: UITextField!
    
    
    
    @IBOutlet weak var OnOffSwitch: LabelSwitch!
    @IBOutlet weak var userImage: UIImageView!
    //@IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnMenu: UIButton!
    
    //MARK:-   Maps Related outlets
    @IBOutlet weak var mapView: GMSMapView!

    //MARK:-  Map relates varibales
    var distanceBetweenTwoCordinate = ""
    var geocodingDataTask: URLSessionDataTask?
    var SearchedLatCordinate = ""
    var SearchedLongCordinate = ""
    var SearchedPlaceName = ""
    var userCordinate:CLLocation?
    var searchedCordinate:CLLocation?
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    
    @IBOutlet weak var btnNavigation: UIButton!
    //MARK:- chat Arr
      var chat_Arr :[chat_model] = []
    //MArk:- user info for pepzi
       
       var user_profileurl = String()
    //MARK:-  viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.btnNavigation.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        hiredUserVIew.addGestureRecognizer(tap)
        hiredUserVIew.isUserInteractionEnabled = true
        self.hiredUserVIew.clipsToBounds = true
        self.FirstsendMessageView.clipsToBounds = true
        self.heightOfHiredUserVIew.constant = 160
        self.heightOfUserDetailsView.constant = 160
        self.heightOfSendMessageView.constant = 0
        self.heightOfUserDetailsViewWithBackButton.constant = 0
        self.bottomConstantOfHiredUserView.constant = -500
        
        let userStatus = UserDefaults.standard.bool(forKey: UserConstant.login_Status)
        if userStatus{
            OnOffSwitch.curState = .R
        }else{
            OnOffSwitch.curState = .L
        }
        OnOffSwitch.delegate = self
        OnOffSwitch.circleShadow = false
        OnOffSwitch.fullSizeTapEnabled = true

        self.navigationController?.navigationBar.isHidden = true
        let Image = UserDefaults.standard.string(forKey: UserConstant.profile_pic)!
        self.userImage.sd_setImage(with: URL(string: Image), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
        
        mapView.clear()
        self.navigationController?.navigationBar.isHidden = true
        let locationServerices = UserLocationManager.SharedManager
        locationServerices.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateLcoationOnMap(_:)), name: NSNotification.Name(rawValue: kLocationDidChangeNotification), object: nil)

        mapView.mapStyle1(withFilename: "map", andType: "json")
        placesClient = GMSPlacesClient.shared()
        mapView.isMyLocationEnabled = true
        self.reloadToCurrentLocationOfuser()
        
        let sessionStatus = UserDefaults.standard.bool(forKey: UserConstant.Session_Status)
        if sessionStatus{
            let size = CGSize(width: 30, height: 30)
            startAnimating(size, message: "Fetching Session...", type: .lineScalePulseOutRapid, fadeInAnimation: nil)
            let sessionId = UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? ""
             let role = UserDefaults.standard.string(forKey: UserConstant.user_type)
            SocketIOManager.sharedInstance.listnerForCheckSession(sessionId: sessionId,type:role!) { (response) in
                print(response)
                self.stopAnimating()
                self.bottomConstantOfHiredUserView.constant = 20
                self.finalHiredUserName2.text = response["name"] as? String ?? ""
                self.finalHiredUserName.text = response["name"] as? String ?? ""
                self.finalHiredUserImage.sd_setImage(with: URL(string: response["image"] as? String ?? "" ), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
                self.user_profileurl = response["image"] as? String ?? ""
                self.finalHiredUserRating.text = response["rating"] as? String ?? ""
                self.finalUserLocation.text = response["session_address"] as? String ?? ""
                var phoneNumber = response["phone"] as? String ?? ""
                let lat = UserDefaults.standard.double(forKey: "Lat")
                let Long = UserDefaults.standard.double(forKey: "Long")
                let myCordinate = CLLocationCoordinate2D(latitude: lat, longitude: Long)
                let distination = CLLocationCoordinate2D(latitude: Double(response["sessionLat"] as? String ?? "")!, longitude: Double(response["sessionLong"] as? String ?? "")!)
                UserDefaults.standard.set(response["sessionId"] as? String ?? "", forKey: UserConstant.Session_id)
                UserDefaults.standard.set(true, forKey: UserConstant.Session_Status)
                UserDefaults.standard.synchronize()
                self.draw(src: myCordinate, dst: distination, mode: "driving")
                self.btnNavigation.isHidden = false
                SocketIOManager.sharedInstance.Joinchatroom(sessionId:response["sessionId"] as? String ?? ""){ (response) in
                    if response.count > 0{
                        if let response = response[0] as? Array<Any>{
                            for dict in response{
                                self.chat_Arr.append(chat_model(dict as! Dictionary))
                            }
                            self.tblChat.reloadData()
                            self.scrollToBottom()
                        }else {
                            self.chat_Arr.append(chat_model(response[0] as! Dictionary))
                            self.tblChat.reloadData()
                            self.scrollToBottom()
                        }
                    }
                    
                }

            }
            if socketConnected{
                 SocketIOManager.sharedInstance.checkSession(sessionId: sessionId, type: role!)
            }


        }


    }
    
    //MARK:-  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        let userId = UserDefaults.standard.string(forKey: UserConstant.user_Id)
        SocketIOManager.sharedInstance.getRequestFromUser(id: userId!) { (response) in
            print(response)
            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "NewRequestVC") as! NewRequestVC
            Vc.modalPresentationStyle = .fullScreen
            Vc.pepziesDetails = response
            Vc.delegate = self
            self.present(Vc, animated: true, completion: nil)
            
        }
        
    }
    
    //MARK:-   delegate method from New Request Screen
    func PaziesAcceptedNewRequest(pazies:[String:Any]) {
        
        let lat = UserDefaults.standard.double(forKey: "Lat")
        let Long = UserDefaults.standard.double(forKey: "Long")

        self.bottomConstantOfHiredUserView.constant = 20
        self.finalHiredUserName2.text = pazies["name"] as? String ?? ""
        self.finalHiredUserName.text = pazies["name"] as? String ?? ""
        self.finalHiredUserImage.sd_setImage(with: URL(string: pazies["image"] as? String ?? "" ), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
        self.finalHiredUserRating.text = pazies["rating"] as? String ?? ""
        self.finalUserLocation.text = pazies["address"] as? String ?? ""
        
        let myCordinate = CLLocationCoordinate2D(latitude: lat, longitude: Long)
        let distination = CLLocationCoordinate2D(latitude: Double(pazies["latitude"] as? String ?? "")!, longitude: Double(pazies["longitude"] as? String ?? "")!)
        self.draw(src: myCordinate, dst: distination, mode: "driving")
        self.btnNavigation.isHidden = false
        UserDefaults.standard.set(pazies["sessionId"] as? String ?? "", forKey: UserConstant.Session_id)
        UserDefaults.standard.set(true, forKey: UserConstant.Session_Status)
        UserDefaults.standard.synchronize()
        
        SocketIOManager.sharedInstance.Joinchatroom(sessionId:pazies["sessionId"] as? String ?? ""){ (response) in
            if response.count > 0{
                if let response = response[0] as? Array<Any>{
                    for dict in response{
                        self.chat_Arr.append(chat_model(dict as! Dictionary))
                    }
                    self.tblChat.reloadData()
                    self.scrollToBottom()
                }else {
                    self.chat_Arr.append(chat_model(response[0] as! Dictionary))
                    self.tblChat.reloadData()
                    self.scrollToBottom()
                }
            }
            
        }
      //  ["estimate_fare": 29, "rating": 0, "longitude": 76.77538899999999, "sessionId": 5efbc7f0-4421-11ea-a449-37db0a4e6acb, "estimate_time": 20, "address": PGIMER, Madhya Marg, Sector 12, Chandigarh, 160012, India, "image": http://3.136.221.59:3000/uploads/profile_image/1578897759488.jpeg, "latitude": 30.76045839999999, "distance": 9.0 , "name": shashi ]
        
    }

    //MARK:-  function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        print("Hello World")
        if self.heightOfHiredUserVIew.constant == 160{
            self.heightOfUserDetailsViewWithBackButton.constant = 70
            self.heightOfSendMessageView.constant = 50
            self.heightOfUserDetailsView.constant = 0
            self.heightOfHiredUserVIew.constant = 432
        }else{
            self.heightOfUserDetailsViewWithBackButton.constant = 0
            self.heightOfSendMessageView.constant = 0
            self.heightOfHiredUserVIew.constant = 160
            self.heightOfUserDetailsView.constant = 160
        }
         UIView.animate(withDuration:0.2, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.layoutIfNeeded()
         }, completion: nil)
    }


    
    
    
    func reloadToCurrentLocationOfuser(){

        let lat = UserDefaults.standard.double(forKey: "Lat")
        let Long = UserDefaults.standard.double(forKey: "Long")
        let camera = GMSCameraPosition.camera(withLatitude: (lat),
                                              longitude: (Long),
                                              zoom: zoomLevel)
        mapView.camera = camera

    }

    //MARK:-   update papzies location
    @objc func UpdateLcoationOnMap(_ notification:Notification) {

        print(notification.userInfo as! [String:Any])
        let Location = notification.userInfo as! [String:Any]
        let current = Location["location"] as! CLLocation
        let camera = GMSCameraPosition.camera(withLatitude: (current.coordinate.latitude),
                                              longitude: (current.coordinate.longitude),
                                              zoom: zoomLevel)
        mapView.camera = camera
        mapView.settings.myLocationButton = true
        mapView.isHidden = false
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    }

}

 //MARK:-   All button action on screen
extension PapziesHomeVC{
    
    @IBAction func cmdFinalUserCancelView(_ sender: Any) {
        
        let alert = UIAlertController(title: "Cancel", message: "Are your sure want to cancel session?",  preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.destructive, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "YES",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.bottomConstantOfHiredUserView.constant = -450
                                        self.btnNavigation.isHidden = true
                                        self.mapView.clear()
                                        self.mapView.mapStyle1(withFilename: "map", andType: "json")
                                        self.mapView.isMyLocationEnabled = true
                                        self.reloadToCurrentLocationOfuser()
                                        let sessionId = UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? ""
                                        SocketIOManager.sharedInstance.cancelAcceptedSession(sessionId: sessionId)
                                        UserDefaults.standard.set("", forKey: UserConstant.Session_id)
                                        UserDefaults.standard.set(false, forKey: UserConstant.Session_Status)


        }))
        
        self.present(alert, animated: true, completion: nil)

        
    }

    @IBAction func cmdNavigation(_ sender: Any) {
                
    }
    @IBAction func cmdMenu(_ sender: Any) {
        sharedMenuClass.menuObj.addMenuClass(view: self.view)
        sharedMenuClass.menuObj.menuView.delegate = self

    }
    
    @IBAction func cmdFinalUserChatOpen(_ sender: Any) {
        self.heightOfUserDetailsViewWithBackButton.constant = 70
        self.heightOfSendMessageView.constant = 50
        self.heightOfUserDetailsView.constant = 0
        self.heightOfHiredUserVIew.constant = 432

    }
    @IBAction func cmdFinalUserCall(_ sender: Any) {
        
    }
    @IBAction func cmdFinalUserBack(_ sender: Any) {
        self.heightOfUserDetailsViewWithBackButton.constant = 0
        self.heightOfSendMessageView.constant = 0
        self.heightOfHiredUserVIew.constant = 160
        self.heightOfUserDetailsView.constant = 160

    }
    @IBAction func cmdSendChat(_ sender: Any) {
        if self.tfsendmessageref.text ?? "" == "" {}else {
            SocketIOManager.sharedInstance.sendmessage(roomId:UserDefaults.standard.string(forKey: UserConstant.Session_id) ?? "",type:"2",message : self.tfsendmessageref.text ?? "")
            self.tfsendmessageref.text = ""
        }
    }


}


 //MARK:-   Location Update delegate method
extension PapziesHomeVC : LocationUpdateProtocol{
    
    func locationDidUpdateToLocation(location: CLLocation) {
        
        self.currentLocation = location
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            DispatchQueue.main.async {
                self.mapView.isHidden = false
                self.mapView.camera = camera

            }
        } else {
            mapView.animate(to: camera)
        }
    }
}

//MARK:-   switch for Change papzies status
extension PapziesHomeVC: LabelSwitchDelegate {
    func switchChangToState(sender: LabelSwitch) {
        let lat = UserDefaults.standard.double(forKey: "Lat")
        let Long = UserDefaults.standard.double(forKey: "Long")

        switch sender.curState {
        case .L: print("on")
        self.setLiveStatus(lat: "\(lat)", long: "\(Long)", stats: "1")
        case .R: print("off")
        self.setLiveStatus(lat: "\(lat)", long: "\(Long)", stats: "0")
        }
    }
}



//MARK:-   Side menu delgates
extension PapziesHomeVC:MenuDelegate {
    
    func callMethod1(vcIndex: Int) {
        navigationClass1(index:vcIndex)
    }
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }

    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

//MARK:-   All Api call in this class
extension PapziesHomeVC{
    
    
     func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D,mode: String){
               
               let config = URLSessionConfiguration.default
               let session = URLSession(configuration: config)
               
               let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=\(mode)&key=AIzaSyBr73R7SkyZkGSbLdeDEwl6n0jPC8SBfJo")!
               let task = session.dataTask(with: url, completionHandler: {
                   (data, response, error) in
                   if error != nil {
                       print(error!.localizedDescription)
                   } else {
                       do {
                           if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
//                               self.estimatedTotalPriceBottomConst.constant = -300
                               let preRoutes = json["routes"] as! NSArray
                               let routes = preRoutes[0] as! NSDictionary
                               let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                               let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                               
                             //Distance
                               let legsDict = routes.value(forKey: "legs") as! NSArray
                               let durationDict = legsDict[0] as! NSDictionary
                               let durationvalueDict:NSDictionary = durationDict.value(forKey: "duration") as! NSDictionary
                               let Exactduration = durationvalueDict.object(forKey: "text") as! String
                             //Time
                               let DistancevalueDict:NSDictionary = durationDict.value(forKey: "distance") as! NSDictionary
                               let ExactDistance = DistancevalueDict.object(forKey: "text") as! String
                               DispatchQueue.main.async(execute: {
                                   if ExactDistance != "" || Exactduration != "" {
                                   }
                                   self.drawline(polyStringd : polyString, src: CLLocationCoordinate2DMake(src.latitude,src.longitude),dst:CLLocationCoordinate2DMake(dst.latitude,dst.longitude))
                               })
                           }
                           
                       } catch {
                           print("parsing error")
                           
                       }
                   }
               })
               task.resume()
           }
        
        func drawline(polyStringd : String,src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D) {
                let cameraPositionCoordinates = CLLocationCoordinate2D(latitude: src.latitude, longitude: src.longitude)
                let cameraPosition = GMSCameraPosition.camera(withTarget: cameraPositionCoordinates, zoom: 12)
                mapView.isMyLocationEnabled = true
                mapView.camera = cameraPosition

            let marker3 = GMSMarker()
                    marker3.icon = UIImage(named: "Drop")
                marker3.iconView?.backgroundColor = UIColor.green
                marker3.iconView?.tintColor = UIColor.green
                marker3.position = CLLocationCoordinate2DMake(dst.latitude,dst.longitude)
                marker3.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                marker3.map = mapView
                
                
                let path = GMSPath(fromEncodedPath: polyStringd)
                let rectangle = GMSPolyline(path: path)
                rectangle.strokeWidth = 4.0
                rectangle.strokeColor = UIColor(named: "#0083CE")!
                rectangle.map = mapView
                
            }
    

    
    func getDistanceBetweenLatLong(myLat:String,myLong:String,la:String, lo:String, completionHandler: @escaping ((_ distance: String) -> Void)){
        

        let distanceApi = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(myLat),\(myLong)&destinations=\(la),\(lo)&key=AIzaSyBr73R7SkyZkGSbLdeDEwl6n0jPC8SBfJo"
        APIClass.apiClass.getRequest(controller: self, contentType: "application/json", method: "get", embedUrl: distanceApi, parameters: [:]) { (response, status) in
        
            print(status)
            print(response as! Dictionary<String,Any>)
            if status{
                if let data = response as? Dictionary <String,Any>{
                    if let desAddress = data["destination_addresses"] as? NSArray{
//                        self.destination_addresses = desAddress[0] as? String ?? ""
                    }
                    if let originAdd = data["origin_addresses"] as? NSArray{
//                        self.origin_addresses  = originAdd[0] as? String ?? ""
                    }
                    if let rows = data["rows"] as? NSArray {
                        if let fistRow = rows[0] as? Dictionary<String,Any>{
                            if let elements = fistRow["elements"] as? NSArray{
                                if let firstElements = elements[0] as? Dictionary<String,Any>{
                                    if let status = firstElements["status"] as? String{
                                        if status == "ZERO_RESULTS"{
//                                            self.noLocation = true
                                            self.Alert(title: "Alert!", message: "Distance not available! please select other location")
                                            return
                                        }
                                    }
                                    if let distance = firstElements["distance"] as? Dictionary<String,Any>{
//                                        self.distanceBetweenPlace = distance["text"] as? String ?? ""
                                        completionHandler(distance["text"] as? String ?? "")
                                    }
                                    if let duration = firstElements["duration"] as? Dictionary<String,Any>{
//                                        self.durationBetweenPlaces = duration["text"] as? String ?? ""
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setLiveStatus(lat:String,long:String,stats:String){
        
        let param:[String:Any] = ["lat":lat,"long":long,"status":stats]
        let BearerToken = UserDefaults.standard.string(forKey: UserConstant.bearer_Token)!

        APIClass.apiClass.PostRequest2(api: "private/live-status", parameters: param, bearerToken: "Bearer \(BearerToken)") { (data, status) in
            
            if status{
                if let response = data as? Dictionary<String,Any>{
                    print(response)
                    if let status = response["status"] as? Int{
                        if status == 0{
                            let message = response["message"] as! String
                            self.Alert(title: "Alert!", message: message)
                            return
                        }else{
                            if stats == "1"{
                                UserDefaults.standard.set(true, forKey: UserConstant.login_Status)

                            }else{
                                UserDefaults.standard.set(false, forKey: UserConstant.login_Status)

                            }
                            UserDefaults.standard.synchronize()
                        }
                    }
                }
            }else{
                self.Alert(title: "Alert!", message: "Something went to wrong")
            }
        }
    }

}

    extension PapziesHomeVC : UITableViewDelegate,UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            self.chat_Arr.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let type = self.chat_Arr[indexPath.row].type
            if "2" == type{
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "sendercell") as! sendercell
                cell1.senderlblref.text = self.chat_Arr[indexPath.row].msg
                let Image = UserDefaults.standard.string(forKey: UserConstant.profile_pic)!
                cell1.senderimgref.sd_setImage(with: URL(string: Image), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
                return cell1
            }else {
                let cell2 = tableView.dequeueReusableCell(withIdentifier: "ReceverCell") as! ReceverCell
                cell2.receverlblref.text  = self.chat_Arr[indexPath.row].msg
                cell2.receverimgref.sd_setImage(with: URL(string:   self.user_profileurl), placeholderImage:UIImage(named: "Ellipse 319"), options: .scaleDownLargeImages, completed: nil)
                return cell2
            }
        }

        
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
      }

      func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
          return 1000.0
     }
        
        func scrollToBottom(){
            let indexPath = NSIndexPath(item: self.chat_Arr.count-1, section: 0)
            self.tblChat.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: false)
        }

    }
